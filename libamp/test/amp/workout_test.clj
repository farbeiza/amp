(ns amp.workout-test
  (:require [clojure.test :refer :all]
            [cljc.java-time.duration :as duration]
            [amp
             [configuration :as conf]
             [workout :as workout]]))

(defn- item
  ([start end] (let [s (duration/of-seconds start), e (duration/of-seconds end)]
                 {:start s, :end e, :duration (.minus e s)}))
  ([start end & kvs] (apply assoc (item start end) kvs)))

(deftest section-sets-test
  (let [section (item 1 6)]
    (is (= [(item 1 3 :section section), (item 3 6 :section section)]
           (#'workout/section-sets [{:duration (duration/of-seconds 2)}
                                    {:duration (duration/of-seconds 3)}]
                                   (duration/of-seconds 1))))))

(deftest sets-test
  (let [section1 (item 0 1), section2 (item 1 6)]
    (is (= [(item 0 1 :section section1)
            (item 1 3 :section section2)
            (item 3 6 :section section2)]
           (workout/sets {:sections [[{:duration (duration/of-seconds 1)}]
                                     [{:duration (duration/of-seconds 2)}
                                      {:duration (duration/of-seconds 3)}]]})))))
