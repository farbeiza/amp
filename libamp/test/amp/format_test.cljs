(ns amp.format-test
  (:require [amp.format :as format]
            [cljs.test :as test]))

(test/deftest zero-pad-test
  (test/testing "should pad with zeroes"
    (test/is (= "00123"
                (format/zero-pad 123 5))))
  (test/testing "should not pad when longer"
    (test/is (= "123"
                (format/zero-pad 123 2)))))

(test/deftest duration-ms-test
  (test/testing "should format duration"
    (test/is (= "20:35"
                (format/duration-ms 1234567.89))))
  (test/testing "should format duration when nil"
    (test/is (= "--:--"
                (format/duration-ms nil)))))

(test/deftest tempo-test
  (test/testing "should format tempo"
    (test/is (= "123 bpm"
                (format/tempo 123))))
  (test/testing "should format tempo when nil"
    (test/is (= "-- bpm"
                (format/tempo nil)))))

(test/deftest rate-test
  (test/testing "should format rate"
    (test/is (= "123.00x"
                (format/rate 123))))
  (test/testing "should format rate when nil"
    (test/is (= "-.--x"
                (format/rate nil)))))
