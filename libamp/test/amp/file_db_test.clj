(ns amp.file-db-test
  (:require [amp
             [db :as db]
             [file-db :as file-db]]
            [clojure.java.io :as io]
            [clojure.test :refer :all])
  (:import [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(def ^:private file-name-fn :f)

(defn update-fn [file value]
  (if (nil? value)
    (hash-map file-name-fn (.getName file))
    (update value :update (fnil inc 0))))

(defn- create-temp-dir-path []
  (.toFile (Files/createTempDirectory "amp-file-db-test-"
                                      (into-array FileAttribute []))))

(defn- create-temp-dir []
  (doto (create-temp-dir-path)
    .deleteOnExit))

(defn- create-dir [& path]
  (doto (apply io/file path)
    .deleteOnExit
    .mkdir))

(defn- create-file [& path]
  (doto (apply io/file path)
    .deleteOnExit
    .createNewFile))

(defn- create-db []
  (let [names ["a" "b" "1" "2"]
        path (create-temp-dir)
        dirs (mapv #(create-dir path %) names)
        keys (doall (for [d dirs, n names] (create-file d n)))
        db (zipmap keys (map update-fn keys (repeat nil)))
        db-files (mapv #(io/file % (#'db/db-file-name)) dirs)]
    {:db db, :path path, :db-files db-files, :keys keys}))

(deftest write-test
  (let [{:keys [db db-files]} (create-db)]
    (doseq [f db-files] (is (not (.exists f))))
    (file-db/write db)
    (doseq [f db-files] (is (.exists f)))))

(deftest read-test
  (let [read-test-fn (fn read-test-fn [n]
                       (let [db-maps (repeatedly n create-db)
                             dbs (map :db db-maps)
                             paths (map :path db-maps)
                             db (apply merge dbs)]
                         (file-db/write db)

                         (is (= db
                                (file-db/read file-name-fn paths)))))]
    (testing "Single path"
      (read-test-fn 1))
    (testing "Multiple paths"
      (read-test-fn 3))))

(deftest update-test
  (let [{initial-db :db, initial-keys :keys} (create-db)
        n 3
        db-keys (drop n initial-keys)
        db (select-keys initial-db db-keys)
        keys (drop-last n initial-keys)
        common-keys (drop n keys)
        update-common-value #(assoc %1 :update 1)
        expected-db (reduce #(update %1 %2 update-common-value)
                            (select-keys initial-db keys)
                            common-keys)]
    (is (not= expected-db db))
    (is (= expected-db
           (db/update db update-fn keys)))))
