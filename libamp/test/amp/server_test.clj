(ns amp.server-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [amp.server :as server]))

(deftest js-escape-test
  (is (= "single \\' double \\\" backslash \\\\"
         (#'server/js-escape "single ' double \" backslash \\"))))

(defn- create-handler []
  (server/wrap (server/resource-routes "/root")))

(deftest handler-test
  (testing "Html"
    (let [handler (create-handler)]
      (is (= {:status 200
              :headers {"Content-Type" "text/html; charset=utf-8"}
              :body "foo=foo\n\n"}
             (handler (mock/request :get "/test.html"))))))
  (testing "Resources"
    (let [handler (create-handler)
          response (handler (mock/request :get "/test.txt"))
          headers (:headers response)
          body (slurp (:body response))]
      (is (= 200 (:status response)))
      (is (= "text/plain; charset=utf-8"
             (get headers "Content-Type")))
      (is (= "4"
             (get headers "Content-Length")))
      (is (= "foo\n" body))))
  (testing "Root"
    (let [handler (create-handler)]
      (is (= {:status 302
              :headers {"Location" "http://localhost/root"
                        "Content-Type" "application/octet-stream"}
              :body ""}
             (handler (mock/request :get "/"))))))
  (testing "Not found"
    (let [handler (create-handler)
          response (handler (mock/request :get "/invalid/path"))]
      (is (= 404
             (:status response))))))
