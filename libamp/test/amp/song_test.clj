(ns amp.song-test
  (:require [clojure.java.io :as io]
            [clojure.test :refer :all]
            [cljc.java-time.duration :as duration]
            [amp
             [song :as song]
             [util :as util]])
  (:import [java.io File]))

(def ^:private test-path (.getPath (io/resource "song.mp3")))
(def ^:private test-file (io/file test-path))
(def ^:private test-duration (duration/of-seconds 8))
(def ^:private test-tempo 80)
(def ^:private test-song {:name (.getName test-file)
                          :last-modified (util/last-modified test-path)
                          :duration test-duration, :tempo test-tempo})

(deftest spy-sh-test
  (testing "Success Exit"
    (let [sh-result {:exit 0, :out "Output", :err "Ignore error"}]
      (is (= sh-result
             (#'song/spy-sh "%s: %s" sh-result)))))
  (testing "Failure Exit"
    (let [sh-result {:exit 1, :out "Output", :err "Ignore error"}]
      (is (= sh-result
             (#'song/spy-sh "%s: %s" sh-result))))))

(deftest duration-test
  (is (= test-duration (#'song/duration test-file))))

(deftest tempo-test
  (is (= test-tempo (#'song/tempo test-file))))

(deftest create-test
  (is (= test-song (song/create test-file))))

(deftest nearest-tempo-test
  (are [e t] (= e (song/nearest-tempo {:tempo t} 100))
    100 50
    100 100
    100 200

    90 45
    90 90
    90 180

    110 55
    110 110
    110 220))

(defn- fuzzy-equals
  ([x y] (fuzzy-equals x y 1e-6))
  ([x y tol] (<= (Math/abs (- x y))
                 (Math/abs (* tol (if (not= x 0.0) x tol))))))

(deftest fuzzy-equals-test
  (testing "Non-zero x"
    (are [e y] (= e (fuzzy-equals 1.0 y 1e-3))
      false 0.99
      true 0.9999
      true 1.0
      true 1.00001
      false 1.01))
  (testing "Zero x"
    (are [e y] (= e (fuzzy-equals 0.0 y 1e-3))
      false -1e-5
      true -1e-6
      true 0.0
      true 1e-6
      false 1e-5)))

(deftest nearest-tempo-rate-test
  (are [e t] (fuzzy-equals e (song/nearest-tempo-rate {:tempo t} 100))
    1.0 50
    1.0 100
    1.0 200

    (/ 100.0 90) 45
    (/ 100.0 90) 90
    (/ 100.0 90) 180

    (/ 100.0 110) 55
    (/ 100.0 110) 110
    (/ 100.0 110) 220))

(deftest duration-at-test
  (are [e n d, t] (= (util/multiply (duration/of-seconds e) (/ n d))
                     (song/duration-at {:duration (duration/of-seconds 20) :tempo 100} t))
    20 1 1, 50
    20 1 1, 100
    20 1 1, 200

    20 100 90, 45
    20 100 90, 90
    20 100 90, 180

    20 100 110, 55
    20 100 110, 110
    20 100 110, 220))
