(ns amp.song-list-test
  (:require [clojure.test :refer :all]
            [amp
             [configuration :as conf]
             [song-list :as song-list]]))

(deftest from-db-test
  (is (= {:queue [:song], :used []}
         (#'song-list/from-db {:key :song}))))

(deftest index-for-tempo-range-test
  (let [songs [{:tempo 150}, {:tempo 70}, {:tempo 100}]]
    (are [e t] (= e (#'song-list/index-for-tempo-range songs t 0.1))
      2 100
      1 200
      0 70
      nil 120)))

(deftest index-for-tempo-test
  (with-redefs [conf/settings (constantly {:tempo-ranges [0.05 0.1]})]
    (let [songs [{:tempo 150}, {:tempo 70}, {:tempo 100}]]
      (are [e t] (= e (#'song-list/index-for-tempo songs t))
        2 100
        1 200
        0 80
        nil 120))))

(deftest extract-for-tempo-test
  (with-redefs [conf/settings (constantly {:tempo-ranges [0.05 0.1]})]
    (let [songs [{:tempo 150}, {:tempo 70}, {:tempo 100}]]
      (are [e t] (= e (#'song-list/extract-for-tempo songs t))
        [{:tempo 100}, [{:tempo 150}, {:tempo 70}]] 100
        [{:tempo 70}, [{:tempo 150}, {:tempo 100}]] 200
        [{:tempo 150}, [{:tempo 70}, {:tempo 100}]] 80
        nil 120))))

(deftest extract-for-tempo-queue-test
  (with-redefs [conf/settings (constantly {:tempo-ranges [0.05 0.1]})]
    (let [queue [{:tempo 150}, {:tempo 70}, {:tempo 100}]
          used [{:tempo 200}]
          song-list {:queue queue, :used used}]
      (are [e t] (= e (#'song-list/extract-for-tempo-queue song-list t))
        [{:tempo 100}, {:queue [{:tempo 150}, {:tempo 70}], :used [{:tempo 200} {:tempo 100}]}] 100
        [{:tempo 70}, {:queue [{:tempo 150}, {:tempo 100}], :used [{:tempo 200} {:tempo 70}]}] 200
        [{:tempo 150}, {:queue [{:tempo 70}, {:tempo 100}], :used [{:tempo 200} {:tempo 150}]}] 80
        nil 120))))

(deftest extract-for-tempo-used-test
  (with-redefs [conf/settings (constantly {:tempo-ranges [0.05 0.1]})]
    (let [queue [{:tempo 200}]
          used [{:tempo 150}, {:tempo 70}, {:tempo 100}]
          song-list {:queue queue, :used used}]
      (are [e t] (= e (#'song-list/extract-for-tempo-used song-list t))
        [{:tempo 100}, {:queue [{:tempo 200}], :used [{:tempo 150}, {:tempo 70}, {:tempo 100}]}] 100
        [{:tempo 70}, {:queue [{:tempo 200}], :used [{:tempo 150}, {:tempo 100} {:tempo 70}]}] 200
        [{:tempo 150}, {:queue [{:tempo 200}], :used [{:tempo 70}, {:tempo 100} {:tempo 150}]}] 80
        nil 120))))

(deftest extract-for-tempo-song-list-test
  (with-redefs [conf/settings (constantly {:tempo-ranges [0.05 0.1]})]
    (let [queue [{:tempo 150}, {:tempo 70}, {:tempo 100}]
          used [{:tempo 170}]
          song-list {:queue queue, :used used}]
      (are [e t] (= e (#'song-list/extract-for-tempo-song-list song-list t))
        [{:tempo 100}, {:queue [{:tempo 150}, {:tempo 70}], :used [{:tempo 170} {:tempo 100}]}] 100
        [{:tempo 70}, {:queue [{:tempo 150}, {:tempo 100}], :used [{:tempo 170} {:tempo 70}]}] 70
        [{:tempo 150}, {:queue [{:tempo 70}, {:tempo 100}], :used [{:tempo 170} {:tempo 150}]}] 80
        [{:tempo 170}, {:queue [{:tempo 150} {:tempo 70}, {:tempo 100}], :used [{:tempo 170}]}] 165
        nil 120))))

(deftest queue?-test
  (are [e s] (= e (#'song-list/queue? s))
    false {:queue [], :used []}
    true {:queue [{:symbol 100}], :used []}))

(deftest used-as-queue-test
  (are [e s] (= e (#'song-list/used-as-queue s))
    {:queue [], :used []} {:queue [], :used []}
    {:queue [{:tempo 100}], :used []} {:queue [{:tempo 100}], :used []}
    {:queue [{:tempo 100}], :used []} {:queue [], :used [{:tempo 100}]}
    {:queue [{:tempo 100} {:tempo 200}], :used []} {:queue [{:tempo 100}], :used [{:tempo 200}]}))

(deftest song-for-tempo-test
  (with-redefs [conf/settings (constantly {:tempo-ranges [0.05 0.1]})]
    (testing "queue"
      (let [queue [{:tempo 150}, {:tempo 70}, {:tempo 100}]
            used [{:tempo 170}]
            song-list {:queue queue, :used used}]
        (are [e t] (= e (song-list/song-for-tempo song-list t))
          [{:tempo 100}, {:queue [{:tempo 150}, {:tempo 70}], :used [{:tempo 170} {:tempo 100}]}] 100
          [{:tempo 70}, {:queue [{:tempo 150}, {:tempo 100}], :used [{:tempo 170} {:tempo 70}]}] 70
          [{:tempo 150}, {:queue [{:tempo 70}, {:tempo 100}], :used [{:tempo 170} {:tempo 150}]}] 80
          [{:tempo 170}, {:queue [{:tempo 150} {:tempo 70}, {:tempo 100}], :used [{:tempo 170}]}] 165
          nil 120)))
    (testing "empty queue"
      (let [queue [{:tempo 100}]
            used [{:tempo 170}]
            song-list {:queue queue, :used used}]
        (are [e t] (= e (song-list/song-for-tempo song-list t))
          [{:tempo 100}, {:queue [{:tempo 170}, {:tempo 100}], :used []}] 100)))))
