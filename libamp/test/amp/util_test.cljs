(ns amp.util-test
  (:require [amp.util :as util]

            [cljs.test :as test]
            [cljc.java-time.duration :as duration]
            [cljc.java-time.instant :as instant]))

(test/deftest duration->s-test
  (test/testing "Zero"
    (test/is (= 0.0
                (util/duration->s duration/zero))))
  (test/testing "Positive"
    (test/is (= 123.456
                (util/duration->s (duration/of-millis 123456)))))
  (test/testing "Negative"
    (test/is (= -123.456
                (util/duration->s (duration/of-millis -123456))))))

(test/deftest hal-expand-uri-test
  (test/is (= "/root/path1?var2=query2"
              (#'util/hal-expand-uri {:_links {:link1 {:href "/root{/var1}{?var2}"}}}
                                     :link1
                                     {:var1 "path1", :var2 "query2"}))))

(test/deftest hal-create-uri-test
  (test/testing "Plain URI"
    (test/is (= "/root/path?var=query"
                (util/hal-create-uri {:_links {:link1 {:href "/root/path?var=query"}}}
                                     :link1))))
  (test/testing "Templated URI"
    (test/is (= "/root/path1?var2=query2"
                (util/hal-create-uri {:_links {:link1 {:href "/root{/var1}{?var2}"}}}
                                     [:link1 {:var1 "path1", :var2 "query2"}])))))

(test/deftest object-reader-test
  (test/testing "Duration"
    (let [duration (duration/of-seconds 10)]
      (test/is (= duration
                  (#'util/object-reader ['java.time.Duration (str duration)]))))
    (test/testing "Instant"
      (let [instant (instant/of-epoch-milli 1234)]
        (test/is (= instant
                    (#'util/object-reader ['java.time.Instant (str instant)])))))))

(test/deftest pr-edn-test
  (let [duration (duration/of-seconds 123456)
        instant (instant/now)
        o {:k1 "v1", :k2 duration, :k3 instant}]
    (test/is
     (= (str "{" ":k1 \"v1\"" ", "
             ":k2 #java.time.Duration \"" duration "\"" ", "
             ":k3 #java.time.Instant \"" instant "\"" "}")
        (util/pr-edn o)))))

(test/deftest read-edn-test
  (test/testing "Automatic EDN Creation"
    (let [content {:k1 "v1", :k2 (duration/of-seconds 123456), :k3 (instant/now)}
          s (util/pr-edn content)]
      (test/is (= content (util/read-edn s)))))
  (test/testing "Manual EDN Creation"
    (let [duration (duration/of-seconds 123456)
          instant (instant/now)
          expected {:k1 "v1", :k2 duration, :k3 instant}]
      (test/are [s] (= expected (util/read-edn s))
        (str "{" ":k1 \"v1\"" ", "
             ":k2 #java.time.Duration \"" duration "\"" ", "
             ":k3 #java.time.Instant \"" instant "\"" "}")
        (str "{" ":k1 \"v1\"" ", "
             ":k2 #object[java.time.Duration \"" duration "\"]" ", "
             ":k3 #object[java.time.Instant \"" instant "\"]" "}")))))
