(ns amp.hal-test
  (:require [clojure.test :refer :all]
            [amp.hal :as hal]))

(deftest resource-add-property-test
  (is (= {:foo "bar"}
         (hal/resource-add-property (hal/resource-create) :foo "bar")))
  (is (= {:foo ["bar" "baz"]}
         (-> (hal/resource-create)
             (hal/resource-add-property , :foo "bar")
             (hal/resource-add-property , :foo "baz")))
      "Multiple resource-add-property for the same key"))

(deftest resource-add-properties-test
  (is (= {:foo "bar", :foobar "foobaz"}
         (hal/resource-add-properties (hal/resource-create)
                                      {:foo "bar", :foobar "foobaz"})))
  (is (= {:foo ["bar" "baz"], :foobar "foobaz"}
         (hal/resource-add-properties (hal/resource-create)
                                      [[:foo "bar"], [:foobar "foobaz"], [:foo "baz"]]))
      "Calling resource-add-properties for the same key"))

(deftest resource-add-href-test
  (is (= {:_links {:foo {:href "/bar/baz"}}}
         (hal/resource-add-href (hal/resource-create)
                                :foo
                                "/bar/baz")))
  (is (= {:_links {:foo [{:href "/bar/baz"}
                         {:href "/foobar/foobaz"}]}}
         (-> (hal/resource-create)
             (hal/resource-add-href , :foo "/bar/baz")
             (hal/resource-add-href , :foo "/foobar/foobaz")))
      "Multiple resource-add-href for the same rel"))

(deftest resource-add-templated-href
  (is (= {:_links {:foo {:href "/bar/{baz}", :templated true}}}
         (hal/resource-add-templated-href (hal/resource-create)
                                          :foo
                                          "/bar/{baz}"))))

(deftest resource-add-resource-test
  (is (= {:_embedded {:foo {:bar "baz"}}}
         (hal/resource-add-resource (hal/resource-create)
                                    :foo
                                    {:bar "baz"})))
  (is (= {:_embedded {:foo [{:bar "baz"}
                            {:foobar "foobaz"}]}}
         (-> (hal/resource-create)
             (hal/resource-add-resource , :foo {:bar "baz"})
             (hal/resource-add-resource , :foo {:foobar "foobaz"})))
      "Multiple resource-add-resource for the same rel"))
