(ns amp.util-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :as io]
            [cljc.java-time
             [duration :as duration]
             [instant :as instant]]
            [amp.util :as util])
  (:import [java.io File]
           [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(deftest remove-nth-test
  (are [e i] (= e (util/remove-nth [:a :b :c] i))
    [:b :c] 0
    [:a :c] 1
    [:a :b] 2
    [:a :b :c] 3))

(deftest move-to-last-test
  (are [e i] (= e (util/move-to-last [:a :b :c] i))
    [:b :c :a] 0
    [:a :c :b] 1
    [:a :b :c] 2))

(deftest index-test
  (testing "Unique keys"
    (is (= {\a "aa", \b "bb", \c "cc"}
           (util/index first
                       ["aa" "bb" "cc"]))))
  (testing "Repeated keys"
    (is (= {\a "aa", \b "bc", \c "cd"}
           (util/index first
                       ["aa" "bb" "cc" "aa" "bc" "cd"])))))

(deftest map-filter-test
  (is (= {:a :a, :d :d}
         (util/map-filter #(= %1 %2)
                          {:a :a, :b 1, :c 2, :d :d}))))

(deftest map-remove-test
  (is (= {:b 1, :c 2}
         (util/map-remove #(= %1 %2)
                          {:a :a, :b 1, :c 2, :d :d}))))

(deftest update-entries-test
  (testing "No extra arguments"
    (is (= {15 -11, 14 -8, 12 -2}
           (util/update-entries {2 13, 3 11, 5 7} (juxt + -)))))
  (testing "Extra arguments"
    (is (= {33 -29, 32 -26, 30 -20}
           (util/update-entries {2 13, 3 11, 5 7} (juxt + -) 7 11)))))

(deftest update-keys-test
  (testing "No extra arguments"
    (is (= {3 :a, 4 :b, 6 :c}
           (util/update-keys {2 :a, 3 :b, 5 :c} inc))))
  (testing "Extra arguments"
    (is (= {20 :a, 21 :b, 23 :c}
           (util/update-keys {2 :a, 3 :b, 5 :c} + 7 11)))))

(deftest update-vals-test
  (testing "No extra arguments"
    (is (= {:a 3, :b 4, :c 6}
           (util/update-vals {:a 2, :b 3, :c 5} inc))))
  (testing "Extra arguments"
    (is (= {:a 20, :b 21, :c 23}
           (util/update-vals {:a 2, :b 3, :c 5} + 7 11)))))

(defn- map-group-by-key-fn [key value]
  (-> key
      io/file
      .getParentFile
      .getName))

(deftest map-group-by-test
  (is (= {"a" {"a/a" :aa, "a/b" :ab}
          "b" {"b/a" :ba}
          "c" {"c/b" :cb,"c/c" :cc}}
         (util/map-group-by map-group-by-key-fn
                            {"a/a" :aa, "b/a" :ba, "c/b" :cb
                             "a/b" :ab,"c/c" :cc}))))

(deftest multiply-test
  (testing "BigDecimal Multiplication"
    (is (= (duration/of-seconds 10)
           (util/multiply (duration/of-seconds 4) 2.5M))))
  (testing "BigInt Multiplication"
    (is (= (duration/of-seconds 10)
           (util/multiply (duration/of-seconds 2) 5N))))
  (testing "BigInteger Multiplication"
    (is (= (duration/of-seconds 10)
           (util/multiply (duration/of-seconds 2) (biginteger 5)))))
  (testing "Double Multiplication"
    (is (= (duration/of-seconds 10)
           (util/multiply (duration/of-seconds 4) 2.5))))
  (testing "Float Multiplication"
    (is (= (duration/of-seconds 10)
           (util/multiply (duration/of-seconds 4) (float 2.5)))))
  (testing "Int Multiplication"
    (is (= (duration/of-seconds 10)
           (util/multiply (duration/of-seconds 2) (int 5)))))
  (testing "Long Multiplication"
    (is (= (duration/of-seconds 10)
           (util/multiply (duration/of-seconds 2) 5))))
  (testing "Ratio Multiplication"
    (is (= (duration/of-seconds 10)
           (util/multiply (duration/of-seconds 4) 5/2)))))

(deftest divide-test
  (testing "BigDecimal Division"
    (is (= (duration/of-seconds 10)
           (util/divide (duration/of-seconds 4) 0.4M))))
  (testing "BigInt Division"
    (is (= (duration/of-seconds 10)
           (util/divide (duration/of-seconds 50) 5N))))
  (testing "BigInteger Division"
    (is (= (duration/of-seconds 10)
           (util/divide (duration/of-seconds 50) (biginteger 5)))))
  (testing "Double Division"
    (is (= (duration/of-seconds 10)
           (util/divide (duration/of-seconds 4) 0.4))))
  (testing "Float Division"
    (is (= (duration/of-seconds 10)
           (util/divide (duration/of-seconds 50) (float 5)))))
  (testing "Int Division"
    (is (= (duration/of-seconds 10)
           (util/divide (duration/of-seconds 50) (int 5)))))
  (testing "Long Division"
    (is (= (duration/of-seconds 10)
           (util/divide (duration/of-seconds 50) 5))))
  (testing "Ratio Division"
    (is (= (duration/of-seconds 10)
           (util/divide (duration/of-seconds 4) 2/5)))))

(deftest appendln-test
  (= "abcd\n"
     (with-out-str (util/appendln *out* "abcd"))))

(deftest file-name-test
  (is (= "bar.ext" (util/file-name "foo/bar.ext"))))

(deftest base-name-test
  (is (= "bar" (util/base-name "foo/bar.ext")))
  (is (= "bar.foobar" (util/base-name "foo/bar.foobar.ext"))))

(defn- create-temp-file ([] (create-temp-file ""))
  ([content]
   (let [file (doto (File/createTempFile "amp-util-test-" ".clj")
                (.deleteOnExit))]
     (spit file content)
     file)))

(deftest last-modified-test
  (let [file (create-temp-file)
        expected (instant/of-epoch-milli 1234567)]
    (.setLastModified file (.toEpochMilli expected))

    (is (= expected
           (util/last-modified file)))))

(defn- create-temp-dir-path []
  (.toFile (Files/createTempDirectory "amp-file-db-test-"
                                      (into-array FileAttribute []))))

(defn- create-temp-dir []
  (doto (create-temp-dir-path)
    .deleteOnExit))

(defn- create-dir [& path]
  (doto (apply io/file path)
    .deleteOnExit
    .mkdir))

(defn- create-file [& path]
  (doto (apply io/file path)
    .deleteOnExit
    .createNewFile))

(deftest delete-file-recursive-test
  (let [names ["a" "b" "1" "2"]
        path (create-temp-dir)
        dirs (mapv #(create-dir path %) names)
        files (doall (for [d dirs, n names] (create-file d n)))
        paths (concat [path] dirs files)]
    (doseq [p paths]
      (is (.exists p)))

    (util/delete-file-recursive path)

    (doseq [p paths]
      (is (not (.exists p))))))

(deftest duration-second-round-test
  (is (= (duration/of-seconds 754)
         (util/duration-second-round (duration/of-seconds 754))))
  (is (= (duration/of-seconds -754)
         (util/duration-second-round (duration/of-seconds -754))))
  (is (= (duration/of-seconds 755)
         (util/duration-second-round (duration/of-millis 754567))))
  (is (= (duration/of-seconds -755)
         (util/duration-second-round (duration/of-millis -754567))))
  (is (= (duration/of-seconds 779)
         (util/duration-second-round (duration/of-seconds 779))))
  (is (= (duration/of-seconds -779)
         (util/duration-second-round (duration/of-seconds -779))))
  (is (= (duration/of-seconds 780)
         (util/duration-second-round (duration/of-millis 779567))))
  (is (= (duration/of-seconds -780)
         (util/duration-second-round (duration/of-millis -779567)))))

(deftest duration->string-test
  (is (= "12:34" (util/duration->string (duration/of-seconds 754))))
  (is (= "-12:34" (util/duration->string (duration/of-seconds -754))))
  (is (= "12:35" (util/duration->string (duration/of-millis 754567))))
  (is (= "-12:35" (util/duration->string (duration/of-millis -754567))))
  (is (= "12:59" (util/duration->string (duration/of-seconds 779))))
  (is (= "-12:59" (util/duration->string (duration/of-seconds -779))))
  (is (= "13:00" (util/duration->string (duration/of-millis 779567))))
  (is (= "-13:00" (util/duration->string (duration/of-millis -779567)))))

(deftest object-reader-test
  (testing "Duration"
    (let [duration (duration/of-seconds 10)]
      (are [xs] (= duration (#'util/object-reader xs))
        ['java.time.Duration (.toString duration)]
        ['java.time.Duration nil (.toString duration)])))
  (testing "Instant"
    (let [instant (instant/of-epoch-milli 1234)]
      (are [xs] (= instant (#'util/object-reader xs))
        ['java.time.Instant (.toString instant)]
        ['java.time.Instant nil (.toString instant)]))))

(defn- create-edn-file [content]
  (create-temp-file (pr-str content)))

(deftest read-edn-test
  (testing "No cache"
    (let [content {:k1 "v1", :k2 "v2"}
          default {:k3 "v3"}
          file (create-edn-file content)]
      (.delete file)
      (is (= default (util/read-edn file default)))))
  (testing "Automatic EDN Creation"
    (let [content {:k1 "v1", :k2 (duration/of-seconds 10), :k3 (instant/now)}
          file (create-edn-file content)]
      (is (= content (util/read-edn file {})))))
  (testing "Manual EDN Creation"
    (let [duration (duration/of-seconds 10)
          instant (instant/now)
          content (str "{" ":k1 \"v1\"" ", "
                       ":k2 #java.time.Duration \"" duration "\"" ", "
                       ":k3 #java.time.Instant \"" instant "\"" "}")
          file (create-temp-file content)]
      (is (= {:k1 "v1", :k2 duration, :k3 instant}
             (util/read-edn file))))))
