(ns amp.song-db-test
  (:require [amp
             [db :as db]
             [song :as song]
             [song-db :as song-db]]
            [cljc.java-time
             [duration :as duration]
             [instant :as instant]]
            [cljc.java-time.temporal.chrono-unit :as chrono-unit]
            [clojure.java.io :as io]
            [clojure.test :refer :all])
  (:import [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]
           [java.util.regex Pattern]))

(def ^:private test-path (.getPath (io/resource "song.mp3")))
(def ^:private test-file (io/file test-path))
(def ^:private test-song (song/create test-file))

(def ^:private song-extension ".mp3")
(defn- song-file-re []
  (re-pattern (str "(?i).*" (Pattern/quote song-extension) "$")))

(def ^:private last-modified (-> (instant/now)
                                 (.truncatedTo , chrono-unit/seconds)))
(def ^:private update-last-modified (.plus last-modified
                                           (duration/of-seconds 1)))

(defn- create-temp-dir-path []
  (.toFile (Files/createTempDirectory "amp-song-db-test-"
                                      (into-array FileAttribute []))))

(defn- create-temp-dir []
  (doto (create-temp-dir-path)
    .deleteOnExit))

(defn- create-dir [& path]
  (doto (apply io/file path)
    .deleteOnExit
    .mkdir))

(defn- create-file [& path]
  (let [copy-test-file #(io/copy test-file %)]
    (doto (apply io/file path)
      .deleteOnExit
      copy-test-file
      (.setLastModified (.toEpochMilli last-modified)))))

(defn- create-files
  ([] (let [names ["a" "b" "1" "2"]
            path (create-temp-dir)
            dirs (mapv #(create-dir path %) names)
            files (doall (for [d dirs, n names] (create-file d (str n song-extension))))]
        [path files]))
  ([n] (reduce #(vector (conj (first %1) (first %2))
                        (concat (second %1) (second %2)))
               [[] []]
               (repeatedly n create-files))))

(deftest song-file-seq-test []
  (testing "Single path"
    (let [[path files] (create-files)]
      (with-redefs [song-db/song-file-re song-file-re]
        (is (= (set files)
               (set (#'song-db/song-file-seq path)))))))
  (testing "Multiple paths"
    (let [[paths files] (create-files 3)]
      (with-redefs [song-db/song-file-re song-file-re]
        (is (= (set files)
               (set (apply #'song-db/song-file-seq paths))))))))

(defn- create-db []
  (let [names ["a" "b" "1" "2"]
        path (create-temp-dir)
        dirs (mapv #(create-dir path %) names)
        keys (doall (for [d dirs, n names] (create-file d (str n song-extension))))
        create-song #(assoc test-song :file %, :name (.getName %), :last-modified last-modified)
        db (zipmap keys (map create-song keys))
        db-files (mapv #(io/file % (#'db/db-file-name)) dirs)]
    {:db db, :path path, :db-files db-files, :keys keys}))

(deftest write-test
  (let [{:keys [db db-files]} (create-db)]
    (doseq [f db-files] (is (not (.exists f))))
    (song-db/write db)
    (doseq [f db-files] (is (.exists f)))))

(deftest read-test
  (let [read-test-fn (fn read-test-fn [n]
                       (let [db-maps (repeatedly n create-db)
                             dbs (map :db db-maps)
                             paths (map :path db-maps)
                             db (apply merge dbs)]
                         (song-db/write db)

                         (is (= db
                                (song-db/read paths)))))]
    (testing "Single path"
      (read-test-fn 1))
    (testing "Multiple paths"
      (read-test-fn 3))))

(defn- create-update-db []
  (let [{initial-db :db, initial-keys :keys, path :path} (create-db)
        n 3
        db-keys (drop n initial-keys)
        db (select-keys initial-db db-keys)
        removed-keys (take-last n initial-keys)
        keys (drop-last n initial-keys)
        common-keys (drop n keys)
        update-common-value #(assoc % :last-modified update-last-modified)
        expected-db (reduce #(update %1 %2 update-common-value)
                            (select-keys initial-db keys)
                            common-keys)]
    (doseq [key (take-last n removed-keys)]
      (.delete key))
    (doseq [key common-keys]
      (.setLastModified key (.toEpochMilli update-last-modified)))

    {:db db, :expected-db expected-db, :path path}))

(deftest update-test
  (let [update-test-fn (fn update-test-fn [n]
                         (let [{:keys [db expected-db path]} (create-update-db)]
                           (is (not= expected-db db))
                           (with-redefs [song-db/song-file-re song-file-re]
                             (dotimes [_ n]
                               (song-db/update [path])))
                           (is (= expected-db
                                  (song-db/read [path])))))]
    (testing "Single update"
      (update-test-fn 1))
    (testing "Two updates"
      (update-test-fn 2))
    (testing "Multiple updates"
      (update-test-fn 3))))
