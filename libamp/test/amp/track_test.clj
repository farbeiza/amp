(ns amp.track-test
  (:require [amp.configuration :as conf]
            [clojure.test :refer :all]
            [cljc.java-time.duration :as duration]
            [amp
             [track :as track]
             [util :as util]]))

(def ^:private song-list {:queue [{:duration (duration/of-seconds 10), :tempo 150}
                                  {:duration (duration/of-seconds 10), :tempo 100}
                                  {:duration (duration/of-seconds 10), :tempo 70}
                                  {:duration (duration/of-seconds 20), :tempo 200}
                                  {:duration (duration/of-seconds 11), :tempo 110}
                                  {:duration (duration/of-seconds 20), :tempo 100}]
                          :used []})

(defn- nth-song [n]
  (nth (:queue song-list) n))

(defn- nth-songs [& ns]
  (mapv nth-song ns))

(defn- track
  ([set start end] {:set set
                    :start start, :end end, :duration (.minus end start)})
  ([song set start end tempo-rate] (assoc (track set start end)
                                          :song (nth-song song)
                                          :tempo-rate tempo-rate)))

(deftest set-tracks-test
  (with-redefs [conf/settings (constantly {:skip 0.1, :skip-last 0.1, :tempo-ranges [0.25]})]
    (let [set {:start (duration/of-seconds 2), :end (duration/of-seconds 42), :tempo 100}]
      (is (= [[(track 1 set (duration/of-seconds 2) (duration/of-seconds 10) 1)
               (track 3 set (duration/of-seconds 10) (duration/of-seconds 26) 1)
               (track 4 set (duration/of-seconds 26) (duration/of-seconds 35 680000000) 10/11)
               (track 5 set (duration/of-seconds 35 680000000) (duration/of-seconds 42) 1)]
              {:queue (nth-songs 0 2)
               :used (nth-songs 1 3 4 5)}]
             (#'track/set-tracks set song-list))))))

(deftest from-song-list-test
  (with-redefs [conf/settings (constantly {:skip 0.1, :skip-last 0.1, :tempo-ranges [0.25]})]
    (let [set1 {:start (duration/of-seconds 2), :end (duration/of-seconds 7)}
          set2 {:start (duration/of-seconds 7), :end (duration/of-seconds 22), :tempo 100}
          set3 {:start (duration/of-seconds 22), :end (duration/of-seconds 47), :tempo 90}
          sets [set1 set2 set3]]
      (is (= [(track set1 (duration/of-seconds 2) (duration/of-seconds 7))
              (track 1 set2 (duration/of-seconds 7) (duration/of-seconds 15) 1)
              (track 3 set2 (duration/of-seconds 15) (duration/of-seconds 22) 1)
              (track 0 set3 (duration/of-seconds 22) (duration/of-seconds 28 666666667) 6/5)
              (track 4 set3 (duration/of-seconds 28 666666667) (duration/of-seconds 39 422222223)
                     9/11)
              (track 5 set3 (duration/of-seconds 39 422222223) (duration/of-seconds 47) 9/10)]
             (#'track/from-song-list sets song-list))))))

(deftest skip-test
  (with-redefs [conf/settings (constantly {:skip 0.1})]
    (is (= (duration/of-millis 3400)
           (track/skip {:song {:duration (duration/of-seconds 10), :tempo 120}
                        :set {:tempo 100}}
                       (duration/of-seconds 2))))))
