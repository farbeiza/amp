(ns amp.db-test
  (:require [amp.db :as db]
            [clojure.java.io :as io]
            [clojure.test :refer :all])
  (:import [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(defn- create-key [& path]
  (.getPath (apply io/file path)))

(defn update-fn [key value]
  (str (or value "")
       (.getName (io/file key))))

(defn- key-fn [db-file-dir val]
  (create-key db-file-dir val))

(defn- create-temp-dir-path []
  (.toFile (Files/createTempDirectory "amp-db-test-"
                                      (into-array FileAttribute []))))

(defn- create-temp-dir []
  (doto (create-temp-dir-path)
    .deleteOnExit))

(defn- create-dir [& path]
  (doto (apply io/file path)
    .deleteOnExit
    .mkdir))

(defn- create-db []
  (let [names ["a" "b" "1" "2"]
        path (create-temp-dir)
        dirs (mapv #(create-dir path %) names)
        keys (for [d dirs, n names] (create-key d n))
        db (into (#'db/empty-db) (zipmap keys (map update-fn keys (repeat nil))))
        db-files (mapv #(io/file % (#'db/db-file-name)) dirs)]
    {:db db, :path path, :db-files db-files, :keys keys}))

(defn- db-file-dir-fn [key val]
  (.getParentFile (io/file key)))

(deftest write-test
  (let [{:keys [db db-files]} (create-db)]
    (doseq [f db-files] (is (not (.exists f))))
    (db/write db db-file-dir-fn)
    (doseq [f db-files] (is (.exists f)))))

(deftest read-test
  (let [read-test-fn (fn read-test-fn [n]
                       (let [db-maps (repeatedly n create-db)
                             dbs (map :db db-maps)
                             paths (map :path db-maps)
                             db (apply merge dbs)]
                         (db/write db db-file-dir-fn)

                         (is (= db
                                (db/read key-fn paths)))))]
    (testing "Single path"
      (read-test-fn 1))
    (testing "Multiple paths"
      (read-test-fn 3))))

(deftest update-test
  (let [{initial-db :db, initial-keys :keys} (create-db)
        n 3
        db-keys (drop n initial-keys)
        db (select-keys initial-db db-keys)
        keys (drop-last n initial-keys)
        common-keys (drop n keys)
        dup #(str % %)
        expected-db (reduce #(update %1 %2 dup)
                            (select-keys initial-db keys)
                            common-keys)]
    (is (not= expected-db db))
    (is (= expected-db
           (db/update db update-fn keys)))))
