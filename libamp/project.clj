(defproject amp/libamp "5.0.1"
  :description "Library Athletic Music Player"
  :plugins [[lein-parent "0.3.8"]
            [lein-cljfmt "0.8.0"]]
  :parent-project {:path "../project.clj"
                   :inherit [:url :license
                             :managed-dependencies
                             :aliases]}
  :dependencies [[org.clojure/clojure]

                 [lambdaisland/uri]

                 [compojure]
                 [ring]
                 [ring/ring-defaults]
                 [ring/ring-jetty-adapter]
                 [ring/ring-json]
                 [cheshire]

                 [selmer]

                 [cljc.java-time]
                 [com.gfredericks/compare]
                 [org.clojure/tools.logging]]
  :profiles {:dev {:dependencies [[ring/ring-mock]]
                   :resource-paths ["dev-resources"]}
             :cljs {:source-paths ["src" "test"]
                    :dependencies [[org.clojure/clojurescript :scope "provided"]
                                   [thheller/shadow-cljs]]}})
