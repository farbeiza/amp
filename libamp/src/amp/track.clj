(ns amp.track
  (:require [amp
             [configuration :as conf]
             [song :as song]
             [song-list :as song-list]
             [util :as util]
             [workout :as wrkt]]
            [clojure.tools.logging :as log]
            [com.gfredericks.compare :as compare]))

(defn- track [track-start {:keys [end tempo] :as set} song-list]
  (let [set-remaining (.minus end track-start)]
    (if (nil? tempo)
      [{:set set, :start track-start, :end end, :duration set-remaining},
       song-list]
      (let [[song sl-n] (song-list/song-for-tempo song-list tempo)
            song-duration (song/duration-at song tempo)
            track-duration (-> song-duration
                               (.minus , (util/multiply song-duration (:skip (conf/settings))))
                               (.minus , (util/multiply song-duration (:skip-last (conf/settings))))
                               (compare/min , set-remaining))
            tempo-rate (song/nearest-tempo-rate song tempo)]
        [{:song song, :set set
          :start track-start, :end (.plus track-start track-duration), :duration track-duration
          :tempo-rate tempo-rate}
         sl-n]))))

(defn- set-tracks [set song-list]
  (loop [s (:start set), sl song-list, ts []]
    (if (compare/>= s (:end set))
      [ts sl]
      (let [[t sl-n] (log/spyf "Created track %s" (track s set sl))]
        (recur (:end t)
               sl-n
               (conj ts t))))))

(defn- from-song-list [sets song-list]
  (loop [ss sets, sl song-list, ts []]
    (if (empty? ss)
      ts
      (let [[tracks sl-n] (set-tracks (first ss) sl)]
        (recur (rest ss) sl-n (into ts tracks))))))

(defn tracks [workout paths]
  (from-song-list (wrkt/sets workout)
                  (song-list/song-list paths)))

(defn skip [track skip]
  (let [{:keys [song set]} track
        song-skip (util/multiply (:duration song) (:skip (conf/settings)))]
    (.plus song-skip
           (song/duration-at song (:tempo set) skip))))
