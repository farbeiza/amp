(ns amp.play.views
  (:require [amp.format :as format]

            [re-frame.core :as rf]
            [cljc.java-time.duration :as duration]
            [goog.string :as gstring]))

(defn workout-view []
  (let [workout @(rf/subscribe [:workout])]
    [:div.h1.m-1
     (:name workout)]))

(def ^:private duration->ms (fnil duration/to-millis duration/zero))

(defn- render-progress
  ([item paused class]
   (render-progress item paused class nil))
  ([{:keys [elapsed remaining duration]} paused class pace-content]
   (let [elapsed-ms (duration->ms elapsed)
         duration-ms (duration->ms duration)
         width (if (= duration-ms 0)
                 0
                 (* (/ elapsed-ms duration-ms) 100))]
     [:div.progress
      [:div {:style {:width (str width "%")}
             :aria-valuenow elapsed-ms
             :aria-valuemin 0, :aria-valuemax duration-ms
             :role "progressbar"
             :class ["progress-bar"
                     (if paused "progress-bar-striped")
                     (if paused "progress-bar-animated")
                     class]}]
      [:div.elapsed.progress-text
       "-" (format/duration remaining)
       " / "
       "+" (format/duration elapsed)]
      (if (some? pace-content)
        (into [:div.pace.progress-text]
              pace-content))
      [:div.duration.progress-text
       (format/duration duration)]])))

(defn- section-view []
  (let [paused @(rf/subscribe [:paused])
        section @(rf/subscribe [:section])]
    [:div.section
     (render-progress section paused "bg-danger")]))

(defn- set-view []
  (let [paused @(rf/subscribe [:paused])
        {:keys [name tempo] :as set} @(rf/subscribe [:set])]
    [:div.set
     [:div {:class [(if paused "paused") "h2" "text-center"]} name]
     (render-progress set paused "bg-warning"
                      [(format/tempo tempo)])]))

(defn- track-view []
  (let [paused @(rf/subscribe [:paused])
        {:keys [name tempo tempo-rate] :as track} @(rf/subscribe [:track])]
    [:div.set
     [:div {:class [(if paused "paused") "h5" "text-center"]} name]
     (render-progress track paused "bg-success"
                      [(format/rate tempo-rate)
                       " "
                       (format/tempo tempo)])]))

(defn- next-view []
  (let [next-set @(rf/subscribe [:next-set])
        next-track @(rf/subscribe [:next-track])]
    [:div.next.d-flex.justify-content-end
     [:div.title.h5 "Next:"]
     [:div
      [:div.h5
       (:name next-set)
       [:span.h6 " (" (format/tempo (:tempo next-set)) ")"]]
      [:div.h6 (:name next-track)]]]))

(defn progress-view []
  [:div.total-progress
   [section-view]
   [set-view]
   [track-view]
   [next-view]])

(defn- back-view []
  [:div.back
   [:a.btn.btn-link {:href "workouts.html"} "Back"]])

(defn- reset-view []
  [:div.reset
   [:button.btn.btn-link
    {:on-click (fn [e]
                 (.preventDefault e)
                 (rf/dispatch [:reset nil]))}
    "Reset"]])

(defn- pause-view []
  (let [paused @(rf/subscribe [:paused])
        on-click (fn [e]
                   (.preventDefault e)
                   (rf/dispatch [:pause]))]
    (if paused
      [:button.btn.btn-success.btn-lg
       {:on-click on-click, :auto-focus true}
       "▶"
       (gstring/unescapeEntities "&nbsp;&nbsp;")
       "Play"]
      [:button.btn.btn-warning.btn-lg
       {:on-click on-click, :auto-focus true}
       "▮▮"
       (gstring/unescapeEntities "&nbsp;&nbsp;")
       "Pause"])))

(defn controls-view []
  [:div.controls
   [back-view]
   [reset-view]
   [pause-view]])
