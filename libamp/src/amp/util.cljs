(ns amp.util
  (:require [cljc.java-time.duration :as duration]
            [cljc.java-time.instant :as instant]
            [clojure.edn :as edn]
            ["@js-joda/core" :as jsjoda]
            ["uri-template" :as uri-template]))

(defn duration->s [d]
  (+ (duration/get-seconds d)
     (* (duration/get-nano d)
        1e-9)))

(defn hal-links [resource]
  (:_links resource))

(defn hal-link [resource link]
  (get (hal-links resource) (keyword link)))

(defn hal-uri [resource link]
  (:href (hal-link resource link)))

(defn- hal-expand-uri [resource link context]
  (.expand (uri-template/parse (hal-uri resource link))
           (clj->js context)))

(defn hal-create-uri [resource args]
  (let [link (keyword args)]
    (if (nil? link)
      (apply hal-expand-uri resource args)
      (hal-uri resource link))))

(defn- write-joda [writer o symbol]
  (-write writer "#java.time.")
  (-write writer (name symbol))
  (-write writer " \"")
  (-write writer (str o))
  (-write writer "\""))

(def ^:private duration-symbol 'java.time.Duration)
(def ^:private instant-symbol 'java.time.Instant)

(extend-type jsjoda/Duration
  IPrintWithWriter
  (-pr-writer [o writer opts]
    (write-joda writer o duration-symbol)))

(extend-type jsjoda/Instant
  IPrintWithWriter
  (-pr-writer [o writer opts]
    (write-joda writer o instant-symbol)))

(defn pr-edn [o]
  (pr-str o))

(declare ^:private edn-readers)

(defn- object-reader [args]
  (let [s (first args), v (last args)
        edn-reader (get edn-readers s)]
    (edn-reader v)))

(def ^:private edn-readers {duration-symbol duration/parse
                            instant-symbol instant/parse
                            'object object-reader})

(def ^:private edn-read-opts {:readers edn-readers})

(defn read-edn [s]
  (edn/read-string edn-read-opts s))
