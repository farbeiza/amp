(ns amp.file-db
  (:require [amp.db :as db]
            [clojure.java.io :as io])
  (:refer-clojure :exclude [read update]))

(defn read [file-name-fn paths]
  (let [key-fn #(io/file %1 (file-name-fn %2))]
    (db/read key-fn paths)))

(defn- db-file-dir-fn [key val]
  (.getParentFile key))

(defn write [db]
  (db/write db db-file-dir-fn))

(defn update [db update-fn files]
  (db/update db update-fn files))
