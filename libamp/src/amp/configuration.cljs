(ns amp.configuration
  (:require [amp.util :as util]))

(let [value (util/read-edn (.-settings js/AMP))]
  (defn settings []
    value))
