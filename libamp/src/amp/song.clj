(ns amp.song
  (:require [amp
             [configuration :as conf]
             [util :as util]]
            [clojure
             [edn :as edn]
             [string :as string]]
            [clojure.java.shell :as shell]
            [cljc.java-time.duration :as duration]
            [clojure.tools.logging :as log]))

(def ^:private temp-prefix "amp")
(def ^:private out-file-format "%s%s_medianTempo.txt")

(defn- split-lines [s] (string/split s #"\n"))

(defn- spy-sh [format {:keys [exit out err] :as sh-result}]
  (log/debugf format "out" out)
  (when-not (string/blank? err)
    (log/logf (if (= exit 0) :debug :warn)
              format "error" err))
  sh-result)

(defn- duration [file]
  (->> (.getPath file)
       (conj (:mp3info-command (conf/settings)))
       (apply shell/sh)
       (spy-sh "mp3info %s: %s")
       :out
       split-lines
       last
       edn/read-string
       duration/of-seconds))

(defn- tempo [file]
  (let [out-attributes (into-array java.nio.file.attribute.FileAttribute [])
        out-dir (java.nio.file.Files/createTempDirectory temp-prefix out-attributes)
        out-prefix (str out-dir (.getSeparator (.getFileSystem out-dir)))]
    (spy-sh "ibt %s: %s" (apply shell/sh (conj (:ibt-command (conf/settings))
                                               (.getPath file)
                                               out-prefix)))
    (let [out-file (format out-file-format out-prefix (util/base-name file))
          tempo (edn/read-string (slurp out-file))]
      (util/delete-file-recursive out-prefix)
      tempo)))

(defn create [file]
  (log/trace "Creating song for" file "...")
  (log/spyf "Created song %s"
            {:name (.getName file), :last-modified (util/last-modified file)
             :duration (duration file), :tempo (tempo file)}))

(defn nearest-tempo [{song-tempo :tempo} tempo]
  (let [op (if (>= tempo song-tempo) * /)
        c (Math/round (double (op (/ tempo song-tempo))))]
    (op song-tempo c)))

(defn nearest-tempo-rate [song tempo]
  (/ tempo (nearest-tempo song tempo)))

(defn duration-at
  ([song tempo] (duration-at song tempo (:duration song)))
  ([song tempo duration]
   (util/divide duration (nearest-tempo-rate song tempo))))
