(ns amp.util
  (:require [cljc.java-time
             [duration :as duration]
             [instant :as instant]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.string :as string])
  (:import [clojure.lang BigInt Ratio]
           [java.io FileNotFoundException PushbackReader]
           [java.time Duration Instant]))

(def nop (constantly nil))

(defn remove-nth [coll index]
  (concat (take index coll)
          (drop (inc index) coll)))

(defn move-to-last [coll index]
  (concat (remove-nth coll index)
          [(nth coll index)]))

(defn index [f coll]
  (into {} (for [x coll] [(f x) x])))

(defn map-filter [pred map]
  (into {} (filter #(apply pred %) map)))

(defn map-remove [pred map]
  (map-filter (complement pred) map))

(defn update-entries [map f & more]
  (into {} (for [[key value] map]
             (apply f key value more))))

(defn update-keys [map f & more]
  (let [entry-fn (fn [key value & more]
                   [(apply f key more) value])]
    (apply update-entries map entry-fn more)))

(defn update-vals [map f & more]
  (let [entry-fn (fn [key value & more]
                   [key (apply f value more)])]
    (apply update-entries map entry-fn more)))

(defn map-group-by [f map]
  (update-vals (group-by #(apply f %) map)
               #(into {} %)))

(derive BigInt ::integer)
(derive BigInteger ::integer)
(derive Integer ::integer)
(derive Long ::integer)

(defmulti multiply (fn [x y] [(class x) (class y)]))
(defmethod multiply [Duration Long] [x y] (.multipliedBy x y))
(defmethod multiply [Duration ::integer] [x y] (multiply x (long y)))
(defmethod multiply [Duration Ratio]
  [x y] (-> x
            (multiply , (numerator y))
            (.dividedBy , (long (denominator y)))))
(defmethod multiply [Duration Number] [x y] (multiply x (rationalize y)))

(prefer-method multiply [Duration ::integer] [Duration Number])

(defn divide [x y] (multiply x (/ y)))

(defn appendln [writer string]
  (.append writer string)
  (.append writer "\n")
  (.flush writer))

(defn file-name [file]
  (.getName (io/file file)))

(defn base-name [file]
  (string/replace (file-name file) #"\.[^.]*$" ""))

(defn last-modified [file]
  (-> (io/file file)
      .lastModified
      instant/of-epoch-milli))

(defn delete-file-recursive [file]
  (doseq [child (reverse (file-seq (io/file file)))]
    (io/delete-file child)))

(def ^:private half-second (duration/of-millis 500))

(defn duration-second-round [duration]
  (-> duration
      (.plus , half-second)
      .getSeconds
      duration/of-seconds))

(defn duration->string [duration]
  (let [rounded-duration (duration-second-round duration)
        minutes (.toMinutes rounded-duration)
        minute-duration (duration/of-minutes minutes)
        second-duration (.minus rounded-duration minute-duration)
        seconds (Math/abs (.getSeconds second-duration))]
    (format "%02d:%02d" minutes seconds)))

(defn pr-edn [o]
  (pr-str o))

(defn- class-symbol [class] (symbol (.getName class)))
(def ^:private duration-symbol (class-symbol Duration))
(def ^:private instant-symbol (class-symbol Instant))

(defn- parse-duration [s] (duration/parse s))
(defn- parse-instant [s] (instant/parse s))

(declare ^:private edn-readers)

(defn- object-reader [args]
  (let [s (first args), v (last args)
        edn-reader (get edn-readers s)]
    (edn-reader v)))

(def ^:private edn-readers {duration-symbol parse-duration
                            instant-symbol parse-instant
                            'object object-reader})
(def ^:private edn-read-opts {:readers edn-readers})

(defn- read-edn-opts [file opts]
  (with-open [r (PushbackReader. (io/reader file))]
    (edn/read (merge edn-read-opts opts)
              r)))

(defn read-edn
  ([file] (read-edn-opts file {}))
  ([file default] (try (read-edn-opts file {:eof default})
                       (catch FileNotFoundException exception
                         default))))
