(ns amp.song-db
  (:require [amp
             [configuration :as conf]
             [file-db :as file-db]
             [song :as song]
             [util :as util]]
            [clojure.java.io :as io]
            [com.gfredericks.compare :as compare])
  (:refer-clojure :exclude [read update]))

(defn read [paths]
  (util/update-entries (file-db/read :name paths)
                       (fn [file song] [file (assoc song :file file)])))

(defn write [db]
  (file-db/write (util/update-vals db
                                   #(dissoc % :file))))

(defn- song-file-re []
  (re-pattern (:song-file-re (conf/settings))))

(defn- song-file-seq
  ([path] (filter #(re-matches (song-file-re) (.getName %))
                  (file-seq (io/file path))))
  ([path & more] (mapcat song-file-seq (cons path more))))

(defn- update-fn [file song]
  (if (or (nil? song)
          (compare/> (util/last-modified file)
                     (:last-modified song)))
    (song/create file)
    song))

(defn update [paths]
  (let [song-files (apply song-file-seq paths)]
    (-> (read paths)
        (file-db/update , update-fn song-files)
        write)))
