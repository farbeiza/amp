(ns amp.song-list
  (:require [amp
             [configuration :as conf]
             [song :as song]
             [song-db :as song-db]
             [util :as util]]))

(defn- from-queue [queue]
  {:queue (vec queue), :used []})

(defn- from-db [db]
  (-> db
      vals
      shuffle
      from-queue))

(defn song-list [paths]
  (from-db (song-db/read paths)))

(defn- index-for-tempo-range [songs tempo tempo-range]
  (let [nearest-tempo #(song/nearest-tempo % tempo)
        tempo? #(and (>= tempo (* % (- 1 tempo-range)))
                     (< tempo (* % (+ 1 tempo-range))))
        nearest-tempo? (comp tempo? nearest-tempo)]
    (first (keep-indexed #(if (nearest-tempo? %2) %1)
                         songs))))

(defn- index-for-tempo [songs tempo]
  (->> (:tempo-ranges (conf/settings))
       (map #(index-for-tempo-range songs tempo %))
       (filter some?)
       first))

(defn- extract-for-tempo [songs tempo]
  (let [index (index-for-tempo songs tempo)]
    (if (some? index)
      [(nth songs index), (vec (util/remove-nth songs index))])))

(defn- extract-for-tempo-queue [{:keys [queue used]} tempo]
  (let [[song songs] (extract-for-tempo queue tempo)]
    (if (some? song)
      [song {:queue songs, :used (conj used song)}])))

(defn- extract-for-tempo-used [song-list tempo]
  (let [[song songs] (extract-for-tempo (:used song-list) tempo)]
    (if (some? song)
      [song (assoc song-list :used (conj songs song))])))

(defn- extract-for-tempo-song-list [song-list tempo]
  (let [[song songs] (extract-for-tempo-queue song-list tempo)]
    (if (some? song)
      [song songs]
      (extract-for-tempo-used song-list tempo))))

(defn- queue? [{:keys [queue]}]
  (boolean (seq queue)))

(defn- used-as-queue [{:keys [queue used]}]
  (from-queue (into queue used)))

(defn song-for-tempo [song-list tempo]
  (let [[song s] (extract-for-tempo-song-list song-list tempo)]
    (if (some? song)
      [song
       (if (queue? s) s (used-as-queue s))])))
