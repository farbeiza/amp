(ns amp.workout
  (:require [amp.configuration :as conf]
            [cljc.java-time.duration :as duration]))

(defn- assoc-start-end [item start & kvs]
  (apply assoc item
         :start start
         :end (.plus start (:duration item))
         kvs))

(defn- duration [initial-sets]
  (reduce #(.plus %1 %2)
          (map :duration initial-sets)))

(defn- section [initial-sets start]
  (assoc-start-end {:duration (duration initial-sets)}
                   start))

(defn- section-sets [initial-sets start]
  (let [s (section initial-sets start)]
    (loop [t start, is initial-sets, ss []]
      (if (empty? is)
        ss
        (let [set (assoc-start-end (first is) t :section s)]
          (recur (:end set) (rest is) (conj ss set)))))))

(defn sets [workout]
  (loop [t duration/zero, is (:sections workout), ws []]
    (if (empty? is)
      ws
      (let [ss (section-sets (first is) t)]
        (recur (:end (last ss))
               (rest is)
               (into ws ss))))))

(defn workout [id]
  (let [workouts (:workouts (conf/settings))
        workout (if (nil? id)
                  (first workouts)
                  (first (filter #(= (:id %) id) workouts)))]
    (when (nil? workout)
      (throw (IllegalArgumentException. "Missing workout.")))
    workout))
