(ns amp.settings
  (:require [cljc.java-time.duration :as duration]))

(def ^:private set-duration (-> (duration/of-minutes 30) (.dividedBy , 7)))

(def default {:workouts [{:id "default"
                          :name "Default"
                          :color "#00ff00"
                          :sections [[{:duration (duration/of-seconds 10), :name "Get ready"}]
                                     [{:duration set-duration, :tempo 80, :resistance 1, :name "Warm up"}
                                      {:duration set-duration, :tempo 80, :resistance 2, :name "Keep the pace"}
                                      {:duration set-duration, :tempo 75, :resistance 3, :name "A bit harder"}
                                      {:duration set-duration, :tempo 70, :resistance 4, :name "The summit"}
                                      {:duration set-duration, :tempo 80, :resistance 2, :name "Relax the muscle"}
                                      {:duration set-duration, :tempo 75, :resistance 3, :name "Almost there"}
                                      {:duration set-duration, :tempo 75, :resistance 4, :name "Grand finale"}]
                                     [{:duration (duration/of-seconds 125), :tempo 60, :name "Cool down"}]]}]

              :tempo-ranges [0.10 0.15 0.20 0.25]
              :refresh-period (duration/of-millis 1000)

              :skip 0.1
              :skip-last 0.1

              :song-file-re "(?i).*\\.mp3$"
              :db-file-name ".AMP.clj"

              :vlc-command ["vlc" "--intf" "lua"]
              :mp3info-command ["mp3info" "-p" "%F\n%k kB\n%S"]
              :ibt-command ["ibt" "--offline" "--output" "medianTempo"]

              :api-base-uri "/api/"
              :javascript-base-uri ""

              :profiles {:camp {:port 3333}
                         :wamp {:port 4444}
                         :watch {:port 2222
                                 :api-base-uri "http://localhost:2222/api/"
                                 :javascript-base-uri "http://localhost:9600/"}}})

(defn merge-profile [settings profile]
  (let [key (keyword profile)]
    (as-> settings s
      (get-in s [:profiles key])
      (merge settings s)
      (assoc s :profile key))))
