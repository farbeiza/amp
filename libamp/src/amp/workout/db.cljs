(ns amp.workout.db
  (:require [amp.re-frame.db]

            [re-frame.core :as rf]))

(def default-value :resistance)

(def value-texts {:tempo "Tempo"
                  :resistance "Resistance"})

(defn next-value [value]
  (if (= value :tempo) :resistance :tempo))

(defn string->value [string]
  (let [value (keyword string)]
    (if (contains? value-texts value)
      value
      default-value)))

(defn init-db [value-string]
  {:workouts []
   :value (string->value value-string)})
