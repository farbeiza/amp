(ns amp.workout.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub :workouts
            (fn [current-db _] (:workouts current-db)))

(rf/reg-sub :value
            (fn [current-db _] (:value current-db)))
