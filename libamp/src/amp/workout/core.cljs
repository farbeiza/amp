(ns amp.workout.core
  (:require [amp.workout.db :as db]
            [amp.workout.events :as events]
            [amp.workout.subs :as subs]
            [amp.workout.views :as views]

            [amp.pwa :as pwa]

            [re-frame.core :as rf]
            [reagent.core :as rgnt]
            [reagent.dom :as dom]))

(defn render []
  (dom/render [views/app-view]
              (.getElementById js/document "app")))

(defn run []
  (pwa/register-service-worker!)
  (rf/dispatch-sync [:initialize])
  (rf/dispatch [:request-workouts])
  (render))

(defn ^:dev/after-load clear-cache-and-render! []
  (rf/clear-subscription-cache!)
  (render))
