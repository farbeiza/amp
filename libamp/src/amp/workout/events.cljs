(ns amp.workout.events
  (:require [amp.workout.db :as db]
            [amp.re-frame.events]
            [amp.re-frame.effects :as fx]

            [re-frame.core :as rf]
            [day8.re-frame.http-fx]
            [ajax.core :as ajax]))

(rf/reg-event-fx :initialize
                 [(rf/inject-cofx :url-parameter :value)]
                 (fn [{value-string :url-parameter} _]
                   {:db (db/init-db value-string)}))

(rf/reg-event-fx :request-workouts
                 (fn [_ _]
                   {:fx [[:dispatch [:hal {:route [:workouts]
                                           :on-success [:process-response-workouts]}]]]}))

(rf/reg-event-db :process-response-workouts
                 (fn [current-db [_ response]]
                   (assoc current-db :workouts
                          (get-in response [:_embedded :workouts]))))
