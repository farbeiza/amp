(ns amp.workout.chart
  (:require ["chart.js" :as chart.js]
            [reagent.dom :as dom]))

(def chart-y-min 20)
(def chart-y-max 100)

(defn section-values [sections value]
  (map value sections))

(defn workout-values [workout value]
  (mapcat #(section-values % value) (:sections workout)))

(defn workouts-values [workouts value]
  (->> workouts
       (mapcat #(workout-values % value))
       (remove nil?)))

(defn create-options [workouts value]
  (let [values (workouts-values workouts value)
        max-value (apply max values)
        min-value (apply min values)
        k (/ (- max-value min-value)
             (- chart-y-max chart-y-min))
        min-y (- min-value (* k chart-y-min))
        max-y max-value]
    {:responsive true
     :maintainAspectRatio true
     :scales {:x {:display false}
              :y {:display false
                  :min min-y
                  :max max-y}}
     :plugins {:legend {:display false}}}))

(defn create-data [workout value]
  (let [data (workout-values workout value)
        labels (range (count data))]
    {:labels labels
     :datasets [{:data data
                 :label 'Dataset'
                 :backgroundColor (:color workout)}]}))

(defn create-chart-data [workout options value]
  {:type "bar"
   :data (create-data workout value)
   :options options})

(defn create-workout-chart [component workout options value]
  (let [context (.getContext (dom/dom-node component) "2d")
        chart-data (create-chart-data workout options value)]
    (chart.js/Chart. context (clj->js chart-data))))
