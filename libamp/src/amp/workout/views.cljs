(ns amp.workout.views
  (:require [amp.workout.chart :as chart]
            [amp.workout.db :as db]

            [re-frame.core :as rf]
            [reagent.core :as rgnt]
            [lambdaisland.uri :as uri]))

(defn value-button-view []
  (let [value @(rf/subscribe [:value])
        next-value (db/next-value value)
        url (uri/assoc-query (uri/uri "")
                             :value
                             (name next-value))
        text (next-value db/value-texts)]
    [:div.value-button
     [:a.btn.btn-link {:href (str url)} text]]))

(defn title-view []
  [:h1.display-1 "Workouts"])

(defn play-workout-url [workout]
  (uri/assoc-query (uri/uri "play.html")
                   :workout (:id workout)))

(defn workout-chart-id [workout]
  (str "workout-chart-" (:id workout)))

(defn workout-chart-component [workout options value]
  (rgnt/create-class
   {:display-name        "workout-chart-component"
    :reagent-render      (fn [] [:canvas {:id (workout-chart-id workout)}])
    :component-did-mount #(chart/create-workout-chart % workout options value)}))

(defn render-workout-graph [workout options value]
  [:div.chart
   [workout-chart-component workout options value]])

(defn render-workout [workout options value]
  ^{:key (:id workout)}
  [:a.workout.list-group-item.list-group-item-action.display-2
   {:href (str (play-workout-url workout))}
   [:div.name (:name workout)]
   (render-workout-graph workout options value)])

(defn workouts-view []
  (let [workouts @(rf/subscribe [:workouts])
        value @(rf/subscribe [:value])
        options (chart/create-options workouts value)]
    [:div.list-group
     (map #(render-workout % options value) workouts)]))

(defn app-view []
  [:div
   [value-button-view]
   [title-view]
   [workouts-view]])
