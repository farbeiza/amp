(ns amp.configuration
  (:require [amp
             [util :as util]
             [settings :as settings]]
            [clojure.java.io :as io]))

(let [default (merge settings/default
                     {:log-dir (io/file (System/getProperty "user.home")
                                        ".local" "share" "amp")})

      config-dir (io/file (System/getProperty "user.home") ".config" "amp")
      config-file (io/file config-dir "settings.clj")
      settings-atom (atom (merge default
                                 (util/read-edn config-file {})))]

  (defn merge-file! [file]
    (swap! settings-atom merge (util/read-edn file)))

  (defn merge-profile! [profile]
    (swap! settings-atom settings/merge-profile profile))

  (defn settings []
    @settings-atom))
