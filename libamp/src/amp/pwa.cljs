(ns amp.pwa)

(defn register-service-worker! []
  (when (.-serviceWorker js/navigator)
    (-> (.-serviceWorker js/navigator)
        (.register , "service-worker.js")
        (.then , #(.log js/console "Service worker registered." %)))))
