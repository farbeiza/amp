(ns amp.hal)

(defn- create-or-add [coll x]
  (if (nil? coll)
    x
    (flatten [coll x])))

(defn- update-create-or-add [map key value]
  (update map key #(create-or-add % value)))

(defn- resource-add-value [resource key rel value]
  (update resource
          key
          #(update-create-or-add % rel value)))

(defn resource-add-property [resource key value]
  (update-create-or-add resource key value))

(defn resource-add-properties [resource map]
  (reduce #(apply resource-add-property %1 %2)
          resource
          map))

(defn resource-add-link [resource rel link]
  (resource-add-value resource :_links rel link))

(defn resource-add-href [resource rel href]
  (resource-add-link resource rel {:href (str href)}))

(defn resource-add-templated-href [resource rel href]
  (resource-add-link resource rel {:href (str href), :templated true}))

(defn resource-add-resource [resource rel embedded]
  (resource-add-value resource :_embedded rel embedded))

(defn resource-create
  ([] {})
  ([map] (-> (resource-create)
             (resource-add-properties , map))))
