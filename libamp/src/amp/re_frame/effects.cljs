(ns amp.re-frame.effects
  (:require [amp.configuration :as conf]
            [amp.util :as util]

            [re-frame.core :as rf]
            [ajax.core :as ajax]
            [lambdaisland.uri :as uri]
            [cljc.java-time.duration :as duration]
            [clojure.string :as str]))

(rf/reg-fx :log
           (fn [value]
             (.log js/console (clj->js value))))

(defn- set-interval [f interval-duration]
  (js/setInterval f (duration/to-millis interval-duration)))

(rf/reg-fx :set-interval
           #(apply set-interval %))

(rf/reg-fx :interval-event
           (fn [[event interval-duration]]
             (set-interval #(rf/dispatch event) interval-duration)))

(defn- local-storage-set [key value]
  (->> value
       util/pr-edn
       (.setItem js/localStorage (name key))))

(rf/reg-fx :local-storage-set
           (fn [m]
             (doseq [[k v] m]
               (local-storage-set k v))))

(let [initial-title (.-title js/document)]
  (rf/reg-fx :title
             (fn [value]
               (set! (.-title js/document)
                     (if (str/blank? value)
                       initial-title
                       (str value " - " initial-title))))))

(def ^:private http-fx-options
  {:method :get
   :format (ajax/json-request-format)
   :response-format (ajax/json-response-format {:keywords? true})
   :on-success [:log]
   :on-failure [:log]})

(defn create-http-fx [{:keys [uri] :as args}]
  (let [api-base-uri (:api-base-uri (conf/settings))
        options (merge http-fx-options
                       args
                       {:uri (uri/join api-base-uri uri)})]
    [:http-xhrio options]))
