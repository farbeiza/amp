(ns amp.re-frame.events
  (:require [amp.util :as util]
            [amp.re-frame.effects :as fx]

            [re-frame.core :as rf]))

(rf/reg-event-fx :log
                 (fn [_ [_ value]]
                   {:fx [[:log value]]}))

(rf/reg-event-fx :interval-event
                 (fn [_ [_ & args]]
                   {:fx [[:interval-event args]]}))

(rf/reg-event-fx :hal
                 (fn [_ [_ {:keys [uri] :as options}]]
                   {:fx [(fx/create-http-fx {:uri uri
                                             :on-success [:hal-success options]})]}))

(rf/reg-event-fx :hal-success
                 (fn [_ [_ {:keys [route] :as options} response]]
                   (let [[link & rest] route
                         uri (util/hal-create-uri response link)
                         new-options (assoc options
                                            :uri uri
                                            :route rest)]
                     {:fx [(if (empty? rest)
                             (fx/create-http-fx new-options)
                             [:dispatch [:hal new-options]])]})))
