(ns amp.re-frame.db
  (:require [amp.util :as util]

            [re-frame.core :as rf]
            [cljc.java-time.instant :as instant]
            [lambdaisland.uri :as uri]))

(rf/reg-cofx :now-instant
             (fn [coeffects]
               (assoc coeffects :now-instant (instant/now))))

(defn url-query-map []
  (-> (-> js/window .-location .-href)
      uri/uri
      uri/query-map))

(defn url-parameter [parameter-key]
  (parameter-key (url-query-map)))

(rf/reg-cofx :url-parameter
             (fn [coeffects parameter-key]
               (assoc coeffects
                      :url-parameter
                      (url-parameter parameter-key))))

(defn url-parameters [parameter-keys]
  (select-keys (url-query-map)
               parameter-keys))

(rf/reg-cofx :url-parameters
             (fn [coeffects parameter-keys]
               (assoc coeffects
                      :url-parameters
                      (url-parameters parameter-keys))))

(defn local-storage-get [local-storage-key]
  (->> local-storage-key
       name
       (.getItem js/localStorage)
       util/read-edn))

(rf/reg-cofx :local-storage-get
             (fn [coeffects local-storage-key]
               (assoc coeffects
                      :local-storage-get
                      (local-storage-get local-storage-key))))

(defn local-storage-select [local-storage-keys]
  (reduce #(assoc %1 %2 (local-storage-get %2))
          {}
          local-storage-keys))

(rf/reg-cofx :local-storage-select
             (fn [coeffects local-storage-keys]
               (assoc coeffects
                      :local-storage-select
                      (local-storage-select local-storage-keys))))
