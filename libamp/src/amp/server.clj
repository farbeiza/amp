(ns amp.server
  (:require [amp.configuration :as conf]
            [amp.util :as util]

            [lambdaisland.uri :as uri]
            [compojure.core :refer [defroutes GET] :as compojure]
            [compojure.route :as route]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.defaults :as defaults]
            [ring.middleware.json :as json]
            [ring.util.response :as response]
            [selmer.parser :as parser]
            [selmer.filters :as filters]
            [clojure.java.io :as io])
  (:import [java.io File]
           [java.time Duration Instant]))

(defn get-base-uri [request]
  (assoc (uri/uri "")
         :scheme (name (:scheme request))
         :host (get (:headers request) "host")
         :path (str (:context request) "/")))

(defn response-hal-json [body]
  (-> (response/response body)
      (response/content-type , "application/hal+json")
      (response/charset , "utf-8")))

(defn response-not-found [body]
  (-> (response/response body)
      (response/status , 404)))

(defn- js-escape-character [c]
  (if (or (= c \") (= c \') (= c \\))
    [\\ c]
    [c]))

(defn- js-escape [s]
  (->> s
       str
       (mapcat js-escape-character)
       (apply str)))

(filters/add-filter! :js-escape js-escape)

(parser/set-resource-path! (io/resource "templates"))

(defn- render-html [html]
  (let [configuration-settings (dissoc (conf/settings) :workouts)
        settings-edn (util/pr-edn configuration-settings)
        context (assoc (conf/settings)
                       :settings-edn settings-edn)]

    (parser/render-file html context)))

(defroutes html
  (GET ["/:html", :html #".*\.html"] [html]
    (render-html html)))

(defroutes resources
  (route/resources "/"))

(defn- root [url]
  (compojure/routes
   (GET  "/" [] (response/redirect url 302))))

(defn resource-routes [root-url]
  (compojure/routes html
                    resources
                    (root root-url)
                    (route/not-found "Not Found")))

(cheshire.generate/add-encoder File
                               (fn [file generator]
                                 (.writeString generator
                                               (.getPath file))))

(cheshire.generate/add-encoder Duration
                               (fn [duration generator]
                                 (.writeNumber generator
                                               (.toMillis duration))))
(cheshire.generate/add-encoder Instant
                               (fn [instant generator]
                                 (.writeString generator
                                               (str instant))))

(defn wrap [handler]
  (-> handler
      json/wrap-json-response
      (defaults/wrap-defaults , defaults/api-defaults)))

(defn server [handler options]
  (jetty/run-jetty handler options))
