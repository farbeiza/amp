(ns amp.db
  (:require [amp
             [util :as util]
             [configuration :as conf]]
            [clojure.java.io :as io]
            [clojure.set :as set]
            [clojure.tools.logging :as log])
  (:refer-clojure :exclude [read update]))

(defn- db-file-name []
  (:db-file-name (conf/settings)))

(defn- empty-db []
  (sorted-map))

(defn- read-db-edn-file [key-fn db-file]
  (log/debug "Reading DB from" db-file "...")
  (into (empty-db)
        (util/index key-fn (util/read-edn db-file))))

(defn- read-db-file [key-fn db-file]
  (let [db-file-dir (.getParentFile db-file)
        file-fn #(key-fn db-file-dir %)]
    (read-db-edn-file file-fn db-file)))

(defn- db-file-seq
  ([path] (filter #(= (.getName %) (db-file-name))
                  (file-seq (io/file path))))
  ([path & more] (mapcat db-file-seq (cons path more))))

(defn read [key-fn paths]
  (apply merge (map #(read-db-file key-fn %)
                    (apply db-file-seq paths))))

(defn- write-db-array [db-file db-array]
  (log/debug "Writing DB into" db-file "...")
  (with-open [w (io/writer db-file)]
    (binding [*out* w] (prn db-array))))

(defn- write-db-file [db-file dir-db]
  (write-db-array db-file
                  (map second dir-db)))

(defn- write-dir [db-file-dir dir-db]
  (write-db-file (io/file db-file-dir (db-file-name))
                 dir-db))

(defn write [db db-file-dir-fn]
  (let [by-dir (util/map-group-by db-file-dir-fn db)]
    (doseq [[db-file-dir dir-db] by-dir]
      (write-dir db-file-dir dir-db))))

(defn- dissoc-key [db key]
  (dissoc db
          (log/spyf "Unused key: %s" key)))

(defn- remove-unused-keys [db existing-keys]
  (let [db-keys (set (keys db))
        existing-key-set (set existing-keys)
        unused-keys (set/difference db-keys existing-key-set)]
    (reduce dissoc-key db unused-keys)))

(defn- missing-vals-as-nil [db existing-keys]
  (merge (zipmap existing-keys (repeat nil))
         db))

(defn- update-existing-keys [db update-fn existing-keys]
  (let [f (fn [key value]
            [key (update-fn key value)])]
    (into (empty-db)
          (util/update-entries (missing-vals-as-nil db existing-keys)
                               f))))

(defn update [db update-fn existing-keys]
  (-> db
      (remove-unused-keys , existing-keys)
      (update-existing-keys , update-fn existing-keys)))
