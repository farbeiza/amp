(ns amp.format
  (:require [cljc.java-time.duration :as duration]

            [clojure.pprint :as pp]
            [clojure.string :as str]))

(defn- zero-pad [x n]
  (let [s (str x)
        l (- n (count s))
        p (repeat l "0")]
    (str (str/join p)
         s)))

(defn duration-ms [d]
  (if (nil? d)
    "--:--"
    (let [s (Math/round (/ d 1000))
          minutes (zero-pad (quot s 60) 2)
          seconds (zero-pad (rem (Math/abs s) 60)
                            2)]
      (str minutes ":" seconds))))

(defn duration [d]
  (duration-ms (if (some? d) (duration/to-millis d))))

(defn tempo [t]
  (str (or t "--")
       " bpm"))

(defn rate [r]
  (str (if (nil? r) "-.--"
           (pp/cl-format nil "~,2F" r))
       "x"))
