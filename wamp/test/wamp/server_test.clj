(ns wamp.server-test
  (:require [amp.configuration :as conf]
            [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [wamp.server :as server]
            [cheshire.core :as cheshire]
            [clojure.java.io :as io]))

(def ^:private test-file (.getPath (io/resource "song.mp3")))
(def ^:private test-path (.getParent (io/file test-file)))

(defn- create-handler
  ([] (create-handler {}))
  ([tracks] (create-handler tracks []))
  ([tracks path] (#'server/handler (constantly tracks) [path])))

(defn- parse-json [body]
  (cheshire/parse-string body true))

(defn- remove-links [resource]
  (dissoc resource :_links))

(defn- get-embedded-resources [resource key]
  (->> (get-in resource [:_embedded key])
       (map remove-links)))

(deftest handler-test
  (testing "Api"
    (testing "Workouts"
      (let [workouts [{:id "workout1"}, {:id "workout2"}]
            handler (create-handler)]
        (with-redefs [conf/settings (constantly {:workouts workouts})]
          (let [response (handler (mock/request :get "/api/workouts/"))
                body (parse-json (:body response))]
            (is (= 200 (:status response)))
            (is (= {"Content-Type" "application/hal+json; charset=utf-8"}
                   (:headers response)))
            (is (= workouts
                   (get-embedded-resources body :workouts))))
          (let [expected (first workouts)
                response (handler (mock/request :get (str "/api/workouts/" (:id expected) "/")))
                body (parse-json (:body response))]
            (is (= 200 (:status response)))
            (is (= {"Content-Type" "application/hal+json; charset=utf-8"}
                   (:headers response)))
            (is (= expected
                   (remove-links body)))))))
    (testing "Tracks"
      (let [track [{:foo "bar"
                    :workout "workout-id"
                    :song {:file (io/file "/song/path")}}]
            expected (parse-json (cheshire/generate-string track))
            handler (create-handler track)
            response (handler (mock/request :get (format "/api/workouts/%s/tracks/" "workout-id")))
            body (parse-json (:body response))]
        (is (= 200 (:status response)))
        (is (= {"Content-Type" "application/hal+json; charset=utf-8"}
               (:headers response)))
        (is (= expected
               (get-embedded-resources body :tracks))))
      (let [handler (#'server/handler (fn [workout-id] (throw (IllegalArgumentException.))) [])
            response (handler (mock/request :get (format "/workouts/%s/tracks/" "invalid-workout")))]
        (is (= 404 (:status response)))))
    (testing "Songs"
      (let [handler (create-handler [] test-path)
            response (handler (mock/request :get (format "/api/songs/%s" test-file)))
            headers (:headers response)
            body (slurp (:body response))]
        (is (= 200 (:status response)))
        (is (= "audio/mpeg"
               (get headers "Content-Type")))
        (is (= "4"
               (get headers "Content-Length")))
        (is (= "foo\n" body)))
      (let [handler (create-handler [] test-path)
            response (handler (mock/request :get (format "/api/songs/%s" "/invalid/path")))]
        (is (= 404 (:status response)))))))
