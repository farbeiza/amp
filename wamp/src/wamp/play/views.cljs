(ns wamp.play.views
  (:require [wamp.play.audio :as audio]
            [amp.play.views :as views]))

(defn app-view []
  [:div
   [audio/audio-component]
   [views/workout-view]
   [views/progress-view]
   [views/controls-view]])
