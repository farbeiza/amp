(ns wamp.play.core
  (:require [wamp.play.db :as db]
            [wamp.play.effects :as effects]
            [wamp.play.events :as events]
            [wamp.play.subs :as subs]
            [wamp.play.views :as views]

            [amp.configuration :as conf]

            [re-frame.core :as rf]
            [reagent.dom :as dom]
            [cljc.java-time.duration :as duration]))

(defn- render []
  (dom/render [views/app-view]
              (.getElementById js/document "app")))

(defn run []
  (rf/dispatch-sync [:initialize])
  (rf/dispatch [:init-db])
  (rf/dispatch [:interval-event
                [:update-elapsed]
                (:refresh-period (conf/settings))])
  (render))

(defn ^:dev/after-load clear-cache-and-render! []
  (rf/clear-subscription-cache!)
  (render))
