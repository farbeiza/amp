(ns wamp.play.audio
  (:require [amp.util :as util]

            [re-frame.core :as rf]
            [reagent.core :as rgnt]
            [reagent.dom :as dom]))

(let [audio-dom-component (atom nil)]
  (defn audio-component []
    (rgnt/create-class
     {:display-name        "audio-component"
      :reagent-render      (fn []
                             [:audio "Your browser does not support the audio element."])
      :component-did-mount #(reset! audio-dom-component %)}))

  (defn play [url & {:keys [playback-rate current-time]}]
    (let [audio-node (dom/dom-node @audio-dom-component)
          start-play (fn []
                       (when (some? playback-rate)
                         (set! (.-playbackRate audio-node) playback-rate))
                       (when (some? current-time)
                         (set! (.-currentTime audio-node)
                               (util/duration->s current-time)))
                       (.play audio-node))]
      (set! (.-src audio-node) url)

      (.addEventListener audio-node
                         "canplay"
                         start-play
                         (clj->js {:once true}))

      (.load audio-node)))

  (defn pause []
    (.pause (dom/dom-node @audio-dom-component))))
