(ns wamp.play.subs
  (:require [re-frame.core :as rf]
            [cljc.java-time.duration :as duration]))

(doseq [query-id [:workout :tracks
                  :actions :action-index
                  :paused
                  :start :elapsed]]
  (rf/reg-sub query-id
              (fn [current-db _]
                (get current-db query-id))))

(defn- assoc-durations [{:keys [duration start end] :as item} elapsed]
  (if (some? item)
    (assoc item
           :duration (duration/of-millis duration)
           :elapsed (duration/minus elapsed (duration/of-millis start))
           :remaining (duration/minus (duration/of-millis end) elapsed))))

(rf/reg-sub :section
            (fn [_ _]
              [(rf/subscribe [:set])
               (rf/subscribe [:elapsed])])
            (fn [[set elapsed] _]
              (assoc-durations (:section set) elapsed)))

(rf/reg-sub :set
            (fn [_ _]
              [(rf/subscribe [:track])
               (rf/subscribe [:elapsed])])
            (fn [[track elapsed] _]
              (assoc-durations (:set track) elapsed)))

(rf/reg-sub :track
            (fn [_ _]
              [(rf/subscribe [:action])
               (rf/subscribe [:elapsed])])
            (fn [[action elapsed] _]
              (let [track (:track action)
                    {:keys [name tempo]} (:song track)]
                (-> track
                    (assoc-durations , elapsed)
                    (assoc , :name name
                             :tempo tempo)))))

(rf/reg-sub :action
            (fn [_ _]
              [(rf/subscribe [:actions])
               (rf/subscribe [:action-index])])
            (fn [[actions action-index] _]
              (nth actions action-index nil)))

(rf/reg-sub :next-set
            (fn [_ _]
              (rf/subscribe [:next-track]))
            (fn [next-track _]
              (:set next-track)))

(rf/reg-sub :next-track
            (fn [_ _]
              (rf/subscribe [:next-action]))
            (fn [next-action _]
              (let [track (:track next-action)
                    {:keys [name tempo]} (:song track)]
                (assoc track
                       :name name
                       :tempo tempo))))

(rf/reg-sub :next-action
            (fn [_ _]
              [(rf/subscribe [:actions])
               (rf/subscribe [:action-index])])
            (fn [[actions action-index] _]
              (nth actions (inc action-index) nil)))
