(ns wamp.play.events
  (:require [wamp.play.db :as db]
            [wamp.play.audio :as audio]

            [amp.re-frame.events]
            [amp.re-frame.effects :as fx]
            [amp.format :as format]
            [amp.util :as util]
            [amp.configuration :as conf]

            [re-frame.core :as rf]
            [day8.re-frame.http-fx]
            [ajax.core :as ajax]
            [cljc.java-time.duration :as duration]
            [cljc.java-time.instant :as instant]
            [lambdaisland.uri :as uri]
            [lambdaisland.uri.normalize :as uri-normalize]))

(rf/reg-event-db :initialize #(db/init-db))

(rf/reg-event-fx :init-db
                 [(rf/inject-cofx :local-storage-get :workout)
                  (rf/inject-cofx :url-parameter :workout)]
                 (fn [{current-db :db
                       workout :local-storage-get, new-workout-id :url-parameter}
                      _]
                   (if (or (nil? workout)
                           (and (some? new-workout-id)
                                (not= new-workout-id (:id workout))))
                     {:fx [[:dispatch [:update-tracks]]]}
                     {:db (assoc current-db :workout workout)
                      :fx [[:dispatch [:init-tracks]]]})))

(rf/reg-event-fx :update-elapsed
                 [(rf/inject-cofx :now-instant)]
                 (fn [{{:keys [paused start-instant elapsed] :as current-db} :db, now :now-instant} _]
                   (if-not paused
                     (let [elapsed (if (nil? start-instant)
                                     elapsed
                                     (duration/between start-instant now))]
                       {:db (assoc current-db :elapsed elapsed)
                        :fx [[:local-storage-set {:elapsed elapsed}]]}))))

(rf/reg-event-fx :init-tracks
                 [(rf/inject-cofx :local-storage-get :tracks)]
                 (fn [{current-db :db, tracks :local-storage-get} _]
                   (if (nil? tracks)
                     {:fx [[:dispatch [:update-tracks]]]}
                     {:db (assoc current-db :tracks tracks)
                      :fx [[:dispatch [:init-elapsed]]]})))

(rf/reg-event-fx :init-elapsed
                 [(rf/inject-cofx :local-storage-get :elapsed)]
                 (fn [{current-db :db, elapsed :local-storage-get} _]
                   {:db (if (nil? elapsed)
                          current-db
                          (assoc current-db :elapsed elapsed))
                    :fx [[:dispatch [:init-actions]]]}))

(rf/reg-event-fx :update-tracks
                 [(rf/inject-cofx :local-storage-get :workout)
                  (rf/inject-cofx :url-parameter :workout)]
                 (fn [{workout :local-storage-get, new-workout-id :url-parameter} _]
                   (let [workout-id (or new-workout-id (:id workout))]
                     {:fx [[:dispatch [:hal {:route [:workouts [:workout {:workout workout-id}]]
                                             :on-success [:process-response-workout]}]]]})))

(rf/reg-event-fx :process-response-workout
                 (fn [{current-db :db} [_ response]]
                   (let [tracks-uri (util/hal-uri response :tracks)]
                     {:db (assoc current-db :workout response)
                      :fx [[:local-storage-set {:workout response}]
                           (fx/create-http-fx {:uri (str tracks-uri)
                                               :on-success [:process-response-tracks]})]})))

(rf/reg-event-fx :process-response-tracks
                 (fn [{current-db :db} [_ response]]
                   (let [tracks (get-in response [:_embedded :tracks])]
                     {:db (assoc current-db :tracks tracks)
                      :fx [[:local-storage-set {:tracks tracks
                                                :elapsed duration/zero}]
                           [:dispatch [:init-actions]]]})))

(defn- create-action [track elapsed]
  (let [start (- (:start track) (duration/to-millis elapsed))
        end (- (:end track) (duration/to-millis elapsed))
        started (and (< start 0) (>= end 0))
        skip (if started (Math/abs start) 0)]
    {:track track
     :start (if started 0 start), :end end
     :function #(rf/dispatch [:play-track track skip])}))

(defn- create-actions [tracks elapsed]
  (if (empty? tracks)
    []
    (let [actions (map #(create-action % elapsed) tracks)
          {:keys [end]} (last actions)]
      (conj (vec actions)
            {:start end, :end end, :function #(rf/dispatch [:pause])}))))

(defn- create-action-index [actions]
  (->> actions
       (filter #(< (:end %) 0))
       count))

(rf/reg-event-db :init-actions
                 (fn [{:keys [tracks elapsed] :as current-db} _]
                   (let [actions (create-actions tracks elapsed)]
                     (assoc current-db
                            :actions actions
                            :action-index (create-action-index actions)))))

(rf/reg-event-fx :play
                 [(rf/inject-cofx :now-instant)]
                 (fn [{{:keys [elapsed] :as current-db} :db, play-instant :now-instant} _]
                   {:db (assoc current-db
                               :start-instant (instant/minus play-instant elapsed)
                               :play-instant play-instant
                               :paused false)
                    :fx [[:dispatch [:update-actions]]
                         [:dispatch [:execute-action]]]}))

(rf/reg-event-db :update-actions
                 (fn [{:keys [tracks elapsed] :as current-db} _]
                   (let [actions (create-actions tracks elapsed)]
                     (assoc current-db
                            :actions actions
                            :action-index (dec (create-action-index actions))))))

(rf/reg-event-fx :execute-action
                 [(rf/inject-cofx :now-instant)]
                 (fn [{{:keys [actions action-index play-instant] :as current-db} :db
                       now :now-instant} _]
                   (let [current-action-index (inc action-index)
                         action (nth actions current-action-index)
                         next-action-index (inc current-action-index)
                         {next-action-start :start} (nth actions next-action-index nil)]
                     (if (some? next-action-start)
                       {:db (assoc current-db :action-index current-action-index)
                        :fx [[:log {:message "Executing..."
                                    :action action}]
                             [:execute-action (:function action)]
                             [:schedule-action (instant/plus-millis play-instant
                                                                    next-action-start)]]}))))

(rf/reg-event-fx :stop
                 [(rf/inject-cofx :now-instant)]
                 (fn [{{:keys [elapsed start-instant] :as current-db} :db, now :now-instant} _]
                   (let [elapsed (if (nil? start-instant)
                                   duration/zero
                                   (duration/between start-instant now))]
                     {:db (assoc current-db
                                 :elapsed elapsed
                                 :paused true)
                      :fx [[:clear-schedule-action]
                           [:pause]]})))

(rf/reg-event-fx :pause
                 (fn [{{:keys [paused]} :db} _]
                   {:fx [[:dispatch [(if paused :play :stop)]]]}))

(defn- calculate-current-time [{:keys [song tempo-rate]} skip-ratio skip]
  (duration/of-millis (+ (* (:duration song) skip-ratio)
                         (Math/round (/ skip tempo-rate)))))

(defn- create-title [{:keys [set]}]
  (str (:name set)
       " (" (format/tempo (:tempo set)) ")"))

(rf/reg-event-fx :play-track
                 (fn [_ [_ track skip]]
                   (let [{skip-ratio :skip} (conf/settings)
                         {:keys [song tempo-rate]} track
                         song-url (util/hal-uri track :song)]
                     (if (some? song)
                       {:fx [[:log {:message "Playing..."
                                    :track track}]
                             [:title (create-title track)]
                             [:play [song-url
                                     :playback-rate tempo-rate
                                     :current-time (calculate-current-time track skip-ratio skip)]]]}))))

(rf/reg-event-fx :reset
                 (fn [_ _]
                   {:db (db/init-db)
                    :fx [[:dispatch [:stop]]
                         [:dispatch [:update-tracks]]]}))
