(ns wamp.play.db
  (:require [amp.re-frame.db]

            [cljc.java-time.duration :as duration]))

(defn init-db []
  {:workout nil
   :tracks nil
   :actions nil, :action-index 0
   :paused true
   :play-instant nil, :start-instant nil, :elapsed duration/zero})
