(ns wamp.play.effects
  (:require [wamp.play.audio :as audio]
            [amp.re-frame.effects]

            [re-frame.core :as rf]
            [cljc.java-time.duration :as duration]
            [cljc.java-time.instant :as instant]))

(rf/reg-fx :play #(apply audio/play %))

(rf/reg-fx :pause #(audio/pause))

(rf/reg-fx :execute-action
           (fn [function]
             (function)))

(let [action-timeout (atom nil)]
  (rf/reg-fx :schedule-action
             (fn [at]
               (js/clearTimeout @action-timeout)
               (let [timeout (duration/between (instant/now) at)]
                 (reset! action-timeout
                         (js/setTimeout #(rf/dispatch [:execute-action])
                                        (duration/to-millis timeout))))))

  (rf/reg-fx :clear-schedule-action
             #(js/clearTimeout @action-timeout)))
