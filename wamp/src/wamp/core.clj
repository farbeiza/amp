(ns wamp.core
  (:gen-class)
  (:require [amp.configuration :as conf]
            [wamp.server :as server]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [clojure.tools
             [cli :as cli]
             [logging :as log]])
  (:import [org.slf4j LoggerFactory]
           [ch.qos.logback.classic Level Logger]))

(def ^:private log-levels
  [Level/OFF Level/ERROR Level/WARN Level/INFO Level/DEBUG Level/TRACE Level/ALL])

(def ^:private cli-options
  [["-f" "--file FILE" "configuration file"]
   ["-p" "--profile PROFILE" "profile"
    :default :wamp
    :parse-fn keyword]
   ["-h" "--help"]
   ["-v" "--verbose" "increase verbosity level"
    :default (.indexOf log-levels Level/WARN)
    :assoc-fn (fn assoc [options key value] (update-in options [key] inc))]])

(defn- init-logging [options]
  (.mkdirs (io/file (:log-dir (conf/settings))))

  (let [root (LoggerFactory/getLogger Logger/ROOT_LOGGER_NAME)
        new-level (->> (:verbose options)
                       (min (- (count log-levels) 1))
                       (nth log-levels))]
    (.setLevel root new-level)))

(defn- exit [status message]
  (binding [*out* *err*] (println message))
  (System/exit status))

(defn- usage [summary]
  (str "Usage: wamp.sh [OPTION]... PATH [PATH]...\n"
       "Options:\n"
       summary))

(defn- error-message [errors message]
  (str "Errors parsing command-line options:"
       "\n  " (string/join "\n  " errors)
       "\n\n"
       message))

(defn- start [paths]
  (server/start paths))

(defn -main
  "Amp."
  [& args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]
    (cond
      (contains? options :help) (exit 0 (usage summary))
      (empty? arguments) (exit 1 (usage summary))
      (seq errors) (exit 1 (error-message errors (usage summary))))

    (when (contains? options :file)
      (conf/merge-file! (:file options)))
    (conf/merge-profile! (:profile options))

    (init-logging options)
    (log/debug "Starting...")

    (try (start arguments)
         (finally
           (shutdown-agents))))

  (log/info "End.")
  (System/exit 0))
