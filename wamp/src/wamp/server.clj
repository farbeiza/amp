(ns wamp.server
  (:require [amp
             [configuration :as conf]
             [hal :as hal]
             [server :as server]
             [track :as track]
             [workout :as wrkt]]
            [lambdaisland.uri :as uri]
            [lambdaisland.uri.normalize :as normalize]
            [compojure.core :refer [GET] :as compojure]
            [ring.middleware.partial-content :as partial-content]
            [ring.util
             [mime-type :as mime-type]
             [response :as response]]
            [clojure.java.io :as io])
  (:import [java.nio.file LinkOption NoSuchFileException]))

(defn- real-path [path]
  (-> path
      io/file
      .toPath
      (.toRealPath , (into-array [LinkOption/NOFOLLOW_LINKS]))))

(defn- file-in-any-path [file paths]
  (try
    (let [file-real-path (real-path file)]
      (->> paths
           (map real-path)
           (some #(.startsWith file-real-path %))))
    (catch NoSuchFileException e
      false)))

(defn- root-response [request]
  (let [base-uri (server/get-base-uri request)]
    (-> (hal/resource-create)
        (hal/resource-add-href , :self base-uri)
        (hal/resource-add-href , :workouts (uri/join base-uri "workouts/"))
        server/response-hal-json)))

(defn- workout-resource [workout request]
  (let [base-uri (server/get-base-uri request)
        workout-path (str (normalize/percent-encode (:id workout) :unreserved) "/")
        workout-uri (uri/join base-uri "workouts/" workout-path)]
    (-> (hal/resource-create workout)
        (hal/resource-add-href , :self workout-uri)
        (hal/resource-add-href , :tracks (uri/join workout-uri "tracks/")))))

(defn- workouts-resources [request]
  (->> (:workouts (conf/settings))
       (map #(workout-resource % request))))

(defn- workouts-resource [request]
  (let [base-uri (server/get-base-uri request)
        workouts-uri (uri/join base-uri "workouts/")]
    (-> (hal/resource-create)
        (hal/resource-add-resource , :workouts (workouts-resources request))
        (hal/resource-add-href , :self workouts-uri)
        (hal/resource-add-templated-href , :workout (uri/join workouts-uri "{workout}/")))))

(defn- workouts-response [request]
  (server/response-hal-json (workouts-resource request)))

(defn- workout-response [workout-id request]
  (try
    (-> (wrkt/workout workout-id)
        (workout-resource , request)
        server/response-hal-json)
    (catch IllegalArgumentException e
      (server/response-not-found (format "Workout not found: %s" workout-id)))))

(defn- track-resource [track request]
  (let [base-uri (server/get-base-uri request)
        song-file (get-in track [:song :file])
        song-path (if-not (nil? song-file)
                    (normalize/percent-encode (.getPath song-file) :unreserved))
        track-resource (hal/resource-create track)]
    (if (nil? song-path)
      track-resource
      (hal/resource-add-href track-resource
                             :song
                             (uri/join base-uri "songs/" song-path)))))

(defn- tracks-resources [tracks-fn workout-id request]
  (->> (tracks-fn workout-id)
       (map #(track-resource % request))))

(defn- tracks-resource [tracks-fn workout-id request]
  (let [workout-path (str (normalize/percent-encode workout-id :unreserved) "/")]
    (-> (hal/resource-create)
        (hal/resource-add-resource , :tracks (tracks-resources tracks-fn workout-id request))
        (hal/resource-add-href , :self (uri/join (server/get-base-uri request)
                                                 "workouts/"
                                                 workout-path
                                                 "tracks/")))))

(defn- tracks-response [tracks-fn workout-id request]
  (try
    (server/response-hal-json (tracks-resource tracks-fn workout-id request))
    (catch IllegalArgumentException e
      (server/response-not-found (format "Workout not found: %s" workout-id)))))

(defn- file-response [path]
  (-> (response/file-response path)
      (response/content-type , (mime-type/ext-mime-type path))))

(defn- song-response [path paths]
  (if-not (file-in-any-path path paths)
    (server/response-not-found (format "Song not found: %s" path))
    (file-response path)))

(defn- api [tracks-fn paths]
  (compojure/context "/api" []
    (GET "/" request
      (root-response request))
    (GET "/workouts/" request
      (workouts-response request))
    (GET "/workouts/:workout-id/" [workout-id :as request]
      (workout-response workout-id request))
    (GET "/workouts/:workout-id/tracks/" [workout-id :as request]
      (tracks-response tracks-fn workout-id request))
    (GET "/songs/:path{.*}" [path]
      (song-response path paths))))

(defn- routes [tracks-fn paths]
  (compojure/routes (api tracks-fn paths)
                    (server/resource-routes "workouts.html")))

(defn- tracks [workout-id paths]
  (track/tracks (wrkt/workout workout-id) paths))

(defn- handler
  ([paths] (handler #(tracks % paths) paths))
  ([tracks-fn paths]
   (-> (routes tracks-fn paths)
       partial-content/wrap-partial-content
       server/wrap)))

(defn start [paths]
  (server/server (handler paths)
                 {:port (:port (conf/settings))}))
