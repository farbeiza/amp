#!/bin/bash

set -e
set -x

PROJECTS=("libamp" "camp" "wamp")

SCRIPT_DIR="$(dirname "${0%}")"

TARGET_DIR="${SCRIPT_DIR}/target"

TASK="${1}"

if [ -z "${TASK}" ]; then
    (
        cd "${SCRIPT_DIR}"
        lein
    )

    exit 1
fi

mkdir -p "${TARGET_DIR}"
for project in "${PROJECTS[@]}"; do
    PROJECT_DIR="${SCRIPT_DIR}/${project}"
    (
        cd "${PROJECT_DIR}"
        npm install
        lein "${TASK}"
    )

    UBERJAR=("${PROJECT_DIR}/target/${project}-"*'-standalone.jar')
    UBERJAR="${UBERJAR[-1]}"

    cp -v "${UBERJAR}" "${TARGET_DIR}"
done
