(ns camp.player-test
  (:require [clojure.test :refer :all]
            [camp.player :as player]
            [cljc.java-time.duration :as duration]))

(defrecord TestPlayer [field]
  player/Player
  (player/start [player] (assoc player :started true))
  (player/stop [player] (assoc player :stopped true))
  (player/action [player track skip] {:player player, :track track, :skip skip}))

(defn- item
  ([start end] {:start (duration/of-seconds start), :end (duration/of-seconds end)
                :skip duration/zero})
  ([start end & kvs] (apply assoc (item start end) kvs)))

(defn- nop
  ([start] (player/nop (duration/of-seconds start)))
  ([start end track] (assoc (nop start)
                            :end (duration/of-seconds end)
                            :track track)))

(deftest actions-test
  (let [player (->TestPlayer "field")
        track1 (item 0 2)
        track2 (item 2 4, :song :song1)
        track3 (item 4 7, :song :song2)
        tracks [track1 track2 track3]]
    (testing "No elapsed"
      (is (= [(nop 0 2 track1)
              (item 2 4, :player player, :track track2)
              (item 4 7, :player player, :track track3)]
             (player/actions player tracks duration/zero))))
    (testing "Elapsed into nop"
      (is (= [(nop 0 1 track1)
              (item 1 3, :player player, :track track2)
              (item 3 6, :player player, :track track3)]
             (player/actions player tracks (duration/of-seconds 1)))))
    (testing "Elapsed into song"
      (is (= [(nop -3 -1 track1)
              (item 0 1, :player player, :track track2, :skip (duration/of-seconds 1))
              (item 1 4, :player player, :track track3)]
             (player/actions player tracks (duration/of-seconds 3)))))
    (testing "Elapsed after end"
      (is (= [(nop -8 -6 track1)
              (item -6 -4, :player player, :track track2)
              (item -4 -1, :player player, :track track3)]
             (player/actions player tracks (duration/of-seconds 8)))))))

(deftest duration-test
  (is (= 4
         (player/duration [{:end 1}, {:end 2}, {:end 4}]))))
