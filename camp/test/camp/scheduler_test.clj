(ns camp.scheduler-test
  (:require [clojure.test :refer :all]
            [camp.scheduler :as scheduler])
  (:import [java.util.concurrent CountDownLatch]))

(deftest join-test
  (let [scheduler (agent {:shutdown-promise (promise)})]
    (send scheduler (fn [{:keys [shutdown-promise] :as s}]
                      (deliver shutdown-promise :shutdown)
                      s))
    (is (= :shutdown
           (scheduler/join scheduler)))))
