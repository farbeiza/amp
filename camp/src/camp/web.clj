(ns camp.web
  (:require [camp
             [printer :as printer]
             scheduler]
            [amp
             [configuration :as conf]
             [hal :as hal]
             [server :as server]
             [util :as util]
             [workout :as wrkt]]
            [cljc.java-time
             [duration :as duration]
             [instant :as instant]]
            [lambdaisland.uri :as uri]
            [lambdaisland.uri.normalize :as normalize]
            [compojure.core :refer [GET POST] :as compojure]
            [ring.util.response :as response]))

(defn- root-response [request]
  (let [base-uri (server/get-base-uri request)]
    (-> (hal/resource-create)
        (hal/resource-add-href , :self base-uri)
        (hal/resource-add-href , :workouts (uri/join base-uri "workouts/"))
        (hal/resource-add-href , :play (uri/join base-uri "play/"))
        server/response-hal-json)))

(defn- workout-resource [workout request]
  (let [base-uri (server/get-base-uri request)
        workout-path (str (normalize/percent-encode (:id workout) :unreserved) "/")
        workout-uri (uri/join base-uri "workouts/" workout-path)]
    (-> (hal/resource-create workout)
        (hal/resource-add-href , :self workout-uri))))

(defn- workouts-resources [request]
  (->> (:workouts (conf/settings))
       (map #(workout-resource % request))))

(defn- workouts-resource [request]
  (let [base-uri (server/get-base-uri request)
        workouts-uri (uri/join base-uri "workouts/")]
    (-> (hal/resource-create)
        (hal/resource-add-resource , :workouts (workouts-resources request))
        (hal/resource-add-href , :self workouts-uri)
        (hal/resource-add-templated-href , :workout (uri/join workouts-uri "{workout}/")))))

(defn- workouts-response [request]
  (server/response-hal-json (workouts-resource request)))

(defn- workout-response [workout-id request]
  (try
    (-> (wrkt/workout workout-id)
        (workout-resource , request)
        server/response-hal-json)
    (catch IllegalArgumentException e
      (server/response-not-found (format "Workout not found: %s" workout-id)))))

(defn- play-response [request]
  (let [base-uri (server/get-base-uri request)
        play-uri (uri/join base-uri "play/")]
    (-> (hal/resource-create)
        (hal/resource-add-href , :self base-uri)
        (hal/resource-add-href , :state (uri/join play-uri "state/"))
        (hal/resource-add-href , :pause (uri/join play-uri "pause/"))
        (hal/resource-add-templated-href , :reset (uri/join play-uri "reset/{?workout}"))
        server/response-hal-json)))

(defrecord Printer []
  printer/Printer
  (printer/print-actions [printer actions])
  (printer/print-state [printer action elapsed])
  (printer/print-summary [printer action])
  (printer/print-pause [printer paused])
  (printer/print-stop [printer]))

(defn- destructure-action [actions index]
  (let [action (nth actions index nil)]
    (let [track (:track action)
          set (:set track)
          section (:section set)]
      {:track track, :set set, :section section})))

(defn- times-state [t elapsed]
  (if (nil? t)
    {}
    {:duration (:duration t)
     :elapsed (.minus elapsed (:start t))
     :remaining (.minus (:end t) elapsed)}))

(defn- section-state [section elapsed]
  (times-state section elapsed))

(defn- set-base [set]
  {:name (:name set)
   :tempo (:tempo set)})
(defn- set-state [set elapsed]
  (merge (times-state set elapsed)
         (set-base set)))

(defn- track-base [track]
  (let [song (:song track)]
    {:name (:name song)
     :tempo-rate (:tempo-rate track)
     :tempo (:tempo song)}))
(defn- track-state [track elapsed]
  (merge (times-state track elapsed)
         (track-base track)))

(defn- state-resource [scheduler]
  (let [{:keys [workout actions action-index start paused elapsed]} @scheduler
        elapsed (if paused
                  elapsed
                  (duration/between start (instant/now)))
        {:keys [action track set section]} (destructure-action actions @action-index)
        {next-set :set, next-track :track} (destructure-action actions (inc @action-index))]
    {:workout workout
     :section (section-state section elapsed)
     :set (set-state set elapsed)
     :track (track-state track elapsed)
     :paused paused
     :next-set (set-base next-set)
     :next-track (track-base next-track)}))

(defn- state-response [scheduler]
  (server/response-hal-json (state-resource scheduler)))

(defn- pause [scheduler]
  (camp.scheduler/pause scheduler)
  (state-response scheduler))

(defn- reset [scheduler request]
  (let [workout-id (get-in request [:params :workout])
        workout (if (nil? workout-id)
                  (:workout @scheduler)
                  (wrkt/workout workout-id))]
    (camp.scheduler/reset scheduler workout)
    (state-response scheduler)))

(defn- api [scheduler]
  (compojure/context "/api" []
    (GET "/" request
      (root-response request))
    (GET "/workouts/" request
      (workouts-response request))
    (GET "/workouts/:workout-id/" [workout-id :as request]
      (workout-response workout-id request))
    (GET "/play/" request
      (play-response request))
    (GET "/play/state/" []
      (state-response scheduler))
    (POST "/play/pause/" []
      (pause scheduler))
    (POST "/play/reset/" request
      (reset scheduler request))))

(defn- routes [scheduler]
  (compojure/routes (api scheduler)
                    (server/resource-routes "workouts.html")))

(defn- handler [scheduler]
  (-> (routes scheduler)
      server/wrap))

(defn control [scheduler]
  (server/server (handler scheduler)
                 {:port (:port (conf/settings))
                  :join? false}))
