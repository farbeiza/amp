(ns camp.play.events
  (:require [camp.play.db :as db]
            [amp.re-frame.events]
            [amp.re-frame.effects :as fx]
            [amp.format :as format]

            [re-frame.core :as rf]
            [day8.re-frame.http-fx]
            [ajax.core :as ajax]
            [lambdaisland.uri :as uri]))

(rf/reg-event-db :initialize #(db/init-db))

(rf/reg-event-fx :init-state
                 [(rf/inject-cofx :url-parameter :workout)]
                 (fn [{new-workout-id :url-parameter} _]
                   {:fx [[:dispatch [:hal {:route [:play :state]
                                           :on-success [:process-response-state new-workout-id]}]]]}))

(rf/reg-event-fx :update-state
                 (fn [_ _]
                   {:fx [[:dispatch [:hal {:route [:play :state]
                                           :on-success [:process-response-state nil]}]]]}))

(defn- create-title [{:keys [set]}]
  (str (:name set)
       " (" (format/tempo (:tempo set)) ")"))

(rf/reg-event-fx :process-response-state
                 (fn [{current-db :db} [_ new-workout-id response]]
                   (let [current-workout-id (get-in response [:workout :id])]
                     {:db (merge current-db response)
                      :fx [(if (and (some? new-workout-id)
                                    (not= new-workout-id current-workout-id))
                             [:dispatch [:reset new-workout-id]])
                           [:title (create-title response)]]})))

(rf/reg-event-fx :reset
                 [(rf/inject-cofx :url-parameter :workout)]
                 (fn [{current-workout-id :url-parameter} [_ new-workout-id]]
                   (let [workout-id (or new-workout-id current-workout-id)
                         route [:play [:reset {:workout workout-id}]]]
                     {:fx [[:dispatch [:hal {:route route
                                             :method :post
                                             :on-success [:process-response-state nil]}]]]})))

(rf/reg-event-fx :pause
                 (fn [_ _]
                   {:fx [[:dispatch [:hal {:route [:play :pause]
                                           :method :post
                                           :on-success [:process-response-state nil]}]]]}))
