(ns camp.play.core
  (:require [camp.play.db :as db]
            [camp.play.events :as events]
            [camp.play.subs :as subs]
            [camp.play.views :as views]

            [amp.configuration :as conf]

            [re-frame.core :as rf]
            [reagent.dom :as dom]
            [cljc.java-time.duration :as duration]))

(defn- render []
  (dom/render [views/app-view]
              (.getElementById js/document "app")))

(defn run []
  (rf/dispatch-sync [:initialize])
  (rf/dispatch [:init-state])
  (rf/dispatch [:interval-event
                [:update-state]
                (:refresh-period (conf/settings))])
  (render))

(defn ^:dev/after-load clear-cache-and-render! []
  (rf/clear-subscription-cache!)
  (render))
