(ns camp.play.views
  (:require [amp.play.views :as views]))

(defn app-view []
  [:div
   [views/workout-view]
   [views/progress-view]
   [views/controls-view]])
