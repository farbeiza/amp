(ns camp.play.subs
  (:require [re-frame.core :as rf]
            [cljc.java-time.duration :as duration]))

(doseq [query-id [:paused
                  :workout
                  :next-set :next-track]]
  (rf/reg-sub query-id
              (fn [current-db _] (get current-db query-id))))

(defn- assoc-durations [item]
  (if (some? item)
    (reduce #(update %1 %2 duration/of-millis)
            item
            [:duration :elapsed :remaining])))

(doseq [query-id [:section :set :track]]
  (rf/reg-sub query-id
              (fn [current-db _]
                (assoc-durations (get current-db query-id)))))
