(ns camp.core
  (:gen-class)
  (:require [camp
             [console :as console]
             player
             printer
             scheduler
             [vlc :as vlc]
             [web :as web]]
            [amp
             [configuration :as conf]
             [song-db :as song-db]
             [workout :as wrkt]]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [clojure.tools
             [cli :as cli]
             [logging :as log]])
  (:import [org.slf4j LoggerFactory]
           [ch.qos.logback.classic Level Logger]))

(def ^:private log-levels
  [Level/OFF Level/ERROR Level/WARN Level/INFO Level/DEBUG Level/TRACE Level/ALL])

(def ^:private cli-options
  [["-C" "--no-console" "Disable console interface."]
   ["-f" "--file FILE" "configuration file"]
   ["-h" "--help"]
   ["-p" "--profile PROFILE" "profile"
    :default :camp
    :parse-fn keyword]
   ["-u" "--update" "Update song db. Do not start player."]
   ["-v" "--verbose" "Increase verbosity level"
    :default (.indexOf log-levels Level/WARN)
    :assoc-fn (fn assoc [options key value] (update-in options [key] inc))]
   ["-w" "--workout WORKOUT" "Workout id"]
   ["-W" "--no-web" "Disable web interface."]])

(defn- init-logging [options]
  (.mkdirs (io/file (:log-dir (conf/settings))))

  (let [root (LoggerFactory/getLogger Logger/ROOT_LOGGER_NAME)
        new-level (->> (:verbose options)
                       (min (- (count log-levels) 1))
                       (nth log-levels))]
    (.setLevel root new-level)))

(defn- exit [status message]
  (binding [*out* *err*] (println message))
  (System/exit status))

(defn- usage [summary]
  (str "Usage: camp.sh [OPTION]... COMMAND [COMMAND-ARG]...\n"
       "Options:\n"
       summary))

(defn- error-message [errors message]
  (str "Errors parsing command-line options:"
       "\n  " (string/join "\n  " errors)
       "\n\n"
       message))

(defn- create-printer [options]
  (let [printers (filter some?
                         [(if-not (contains? options :no-console) (console/->Printer))
                          (if-not (contains? options :no-web) (web/->Printer))])]
    (camp.printer/->CompositePrinter printers)))

(defn- play
  ([player printer workout paths options]
   (let [scheduler (camp.scheduler/scheduler player printer workout paths)]
     (try (camp.scheduler/reset scheduler)
          (when-not (contains? options :no-console)
            (console/control scheduler))
          (when-not (contains? options :no-web)
            (web/control scheduler))

          (camp.scheduler/join scheduler)

          (finally (camp.scheduler/shutdown scheduler)))))
  ([paths options]
   (let [workout (wrkt/workout (:workout options))
         printer (create-printer options)
         player (vlc/vlc)]
     (try (play player printer workout paths options)
          (finally (camp.player/shutdown player))))))

(defn -main
  "Console Athletic Music Player"
  [& args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]
    (cond
      (contains? options :help) (exit 0 (usage summary))
      (empty? arguments) (exit 1 (usage summary))
      (seq errors) (exit 1 (error-message errors (usage summary))))

    (when (contains? options :file)
      (conf/merge-file! (:file options)))
    (conf/merge-profile! (:profile options))

    (init-logging options)
    (log/debug "Starting...")

    (if (contains? options :update)
      (song-db/update arguments)
      (try (play arguments options)
           (finally (shutdown-agents)))))

  (log/info "End.")
  (System/exit 0))
