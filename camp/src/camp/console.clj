(ns camp.console
  (:require [camp
             [printer :as printer]
             scheduler]
            [amp
             [util :as util]]
            [cljc.java-time.duration :as duration]
            [clojure.string :as string]))

(defn- print-same-line [& args]
  (print "\r")
  (apply print args)
  (flush))

(defn- print-new-line [& args]
  (apply println args))

(defn- print-action
  ([action track-time set-time section-time]
   (print-action action "" track-time set-time section-time))
  ([action sign track-time set-time section-time]
   (print-action action "" sign track-time set-time section-time))
  ([{:keys [track]} indent sign track-time set-time section-time]
   (let [tempo (get-in track [:set :tempo] 0)
         tempo-rate (double (:tempo-rate track 0))
         name (get-in track [:set :name] "")
         file-name (util/file-name (get-in track [:song :file] ""))
         song-tempo (get-in track [:song :tempo] 0)]
     (print-same-line (format "%s%s%s | %s%s %s (%d bpm) | %s%s %s (%.2fx %d bpm)"
                              indent
                              sign (util/duration->string section-time)
                              sign (util/duration->string set-time) name tempo
                              sign (util/duration->string track-time) file-name
                              tempo-rate song-tempo)))))

(defn- print-summary
  ([action] (print-summary action ""))
  ([action indent] (print-summary action indent "+"))
  ([action indent sign]
   (when (some? (:track action))
     (let [track-elapsed (get-in action [:track :duration] duration/zero)
           track-end (get-in action [:track :end] duration/zero)
           set-elapsed (.minus track-end
                               (get-in action [:track :set :start] duration/zero))
           section-elapsed (.minus track-end
                                   (get-in action [:track :set :section :start] duration/zero))]
       (print-action action indent sign track-elapsed set-elapsed section-elapsed))
     (println))))

(defn- print-pause [paused]
  (when paused
    (println)
    (println)
    (println "Paused. Press enter to continue...")))

(defn- print-stop []
  (println)
  (println)
  (println "Stopped."))

(defrecord Printer []
  printer/Printer
  (printer/print-actions [printer actions]
    (print-new-line "Plan")
    (doseq [action actions] (print-summary action "  ")))
  (printer/print-state [printer action elapsed]
    (let [track-remaining (.minus (get-in action [:track :end] duration/zero) elapsed)
          set-remaining (.minus (get-in action [:track :set :end] duration/zero) elapsed)
          section-remaining (.minus (get-in action [:track :set :section :end] duration/zero)
                                    elapsed)]
      (print-action action "-" track-remaining set-remaining section-remaining)))
  (printer/print-summary [printer action] (print-summary action))
  (printer/print-pause [printer paused] (print-pause paused))
  (printer/print-stop [printer] (print-stop)))

(defn- control-loop [scheduler]
  (while true
    (let [command (string/lower-case (string/trim (read-line)))]
      (case command
        ("" "p" "pause") (camp.scheduler/pause scheduler)
        ("r" "reset") (camp.scheduler/reset scheduler)
        (println (str "Wrong comand: " command "."
                      " Should be one of: pause, reset."))))))

(defn control [scheduler]
  (future (control-loop scheduler)))
