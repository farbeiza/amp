(ns camp.vlc
  (:require [camp
             [player :as player]]
            [amp
             [configuration :as conf]
             [song :as song]
             [track :as track]
             [util :as util]]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log])
  (:import [java.lang ProcessBuilder$Redirect]
           [java.util.concurrent TimeUnit]))

(defn- send-command [{:keys [input-writer output-writer]} command]
  ((fn [c & ws] (doseq [w ws] (util/appendln w c)))
   (log/spyf "Sending VLC command: %s" command)
   output-writer
   input-writer))

(defn- stop [vlc]
  (send-command vlc "stop")
  (send-command vlc "clear"))

(defn- pause [vlc]
  (send-command vlc "pause"))

(defn- play [vlc file]
  (send-command vlc (str "add " (.getPath file)))
  ;; Wait before any other command
  ;; TODO - Change the way of waiting
  (Thread/sleep 100))

(defn- set-volume [vlc]
  (send-command vlc (str "volume " "255")))

(defn- set-tempo-rate [vlc rate]
  (send-command vlc (str "rate " (double rate))))

(defn- seek [vlc duration]
  (send-command vlc (str "seek " (.getSeconds duration))))

(defn- action [vlc {:keys [song tempo-rate] :as track} skip]
  (let [method (fn vlc-action []
                 (play vlc (:file song))
                 (set-volume vlc)
                 (set-tempo-rate vlc tempo-rate)
                 (seek vlc (track/skip track skip)))]
    (log/spyf "Created VLC action %s"
              {:method method})))

(defn- shutdown [{:keys [process input-writer output-writer]}]
  (with-open [iw input-writer, ow output-writer]
    (.destroy process)
    (when-not (.waitFor process 4 TimeUnit/SECONDS)
      (.destroyForcibly process))))

(defrecord Vlc [process input-writer output-writer]
  player/Player
  (player/start [vlc] nil)
  (player/stop [vlc] (stop vlc))
  (player/pause [vlc] (pause vlc))
  (player/action [vlc track skip] (action vlc track skip))
  (player/shutdown [vlc] (shutdown vlc)))

(defn- create-vlc-process []
  (.start (doto (new ProcessBuilder (:vlc-command (conf/settings)))
            (.redirectErrorStream true))))

(defn- redirect-output [from to]
  (future (io/copy from to)))

(defn vlc
  ([] (vlc (io/file (:log-dir (conf/settings)) "vlc.log")))
  ([output-path]
   (let [process (create-vlc-process)
         output-writer (if (some? output-path) (io/writer output-path))]
     (when (some? output-writer)
       (redirect-output (.getInputStream process) output-writer))

     (map->Vlc {:process process
                :input-writer (io/writer (.getOutputStream process))
                :output-writer output-writer}))))
