(ns camp.scheduler
  (:require [camp
             player
             printer]
            [amp
             [configuration :as conf]
             [track :as track]
             [util :as util]]
            [cljc.java-time
             [duration :as duration]
             [instant :as instant]]
            [clojure.tools.logging :as log])
  (:import [java.util.concurrent Executors TimeUnit]))

(def ^:private thread-pool-size 2)

(defn scheduler [player printer workout paths]
  (agent {:player player, :printer printer, :workout workout, :paths paths
          :shutdown-promise (promise)
          :paused true, :action-index (atom 0)}))

(defn- stop-fn [{:keys [player printer executor] :as scheduler} player-stop-fn]
  (player-stop-fn player)
  (when (some? executor)
    (.shutdownNow executor))
  (dissoc scheduler :executor))

(defn- stop-print-fn [previous]
  (let [{:keys [printer] :as scheduler} (stop-fn previous camp.player/stop)]
    (camp.printer/print-stop printer)
    scheduler))

(defn stop [scheduler]
  (send scheduler stop-print-fn))

(defn shutdown [scheduler]
  (stop scheduler)
  (deliver (:shutdown-promise @scheduler) true))

(defn- create-task [f & args]
  (fn task [] (try (apply f args)
                   (catch Exception e
                     (log/warn e "Exception running task")))))

(defn- schedule-delayed [executor delay f & args]
  (.schedule executor
             (apply create-task f args)
             (.toMillis delay) TimeUnit/MILLISECONDS))

(defn- schedule-at-fixed-rate [executor period f & args]
  (.scheduleAtFixedRate executor
                        (apply create-task f args)
                        0 (.toMillis period) TimeUnit/MILLISECONDS))

(defn- print-state [printer actions action-index previous-index workout-start]
  (while (> @action-index @previous-index)
    (camp.printer/print-summary printer (nth actions @previous-index))
    (swap! previous-index inc))
  (when (< @action-index (count actions))
    (let [elapsed (duration/between workout-start (instant/now))]
      (camp.printer/print-state printer (nth actions @action-index) elapsed))))

(defn- schedule-print [executor printer actions action-index workout-start]
  (let [previous-index (atom @action-index)]
    (schedule-at-fixed-rate executor (:refresh-period (conf/settings))
                            print-state printer actions action-index previous-index
                            workout-start)))

(defn- execute-action [actions action-index]
  (locking actions
    (swap! action-index inc)
    (let [{:keys [method]} (nth actions @action-index)]
      (method))))

(defn- schedule-action [executor actions action action-index schedule-start]
  (let [delay (.plus (duration/between (instant/now) schedule-start)
                     (:start action))]
    (log/debug "Scheduling after" delay ":" action)
    (schedule-delayed executor delay
                      execute-action actions action-index)))

(defn- schedule-actions [executor actions action-index schedule-start]
  (mapv #(schedule-action executor actions % action-index schedule-start)
        (drop (inc @action-index) actions)))

(defn- schedule [executor actions action-index printer schedule-start workout-start]
  (let [finished-actions (take-while #(.isNegative (:end %)) actions)]
    (reset! action-index (count finished-actions))
    (schedule-print executor printer actions action-index workout-start)
    (schedule-actions executor actions action-index schedule-start)))

(defn- create-actions [player tracks elapsed end-fn]
  (let [actions (camp.player/actions player tracks elapsed)
        end-action (camp.player/method-action (:end (last actions)) end-fn)]
    (conj (vec actions) end-action)))

(defn- play-fn [{:keys [player printer tracks action-index elapsed] :as scheduler}
                player-stop-fn player-start-fn
                end-fn]
  (stop-fn scheduler player-stop-fn)

  (let [executor (Executors/newScheduledThreadPool thread-pool-size)
        actions (create-actions player tracks elapsed end-fn)
        schedule-start (instant/now)
        workout-start (.minus schedule-start elapsed)]
    (player-start-fn player)
    (camp.printer/print-actions printer actions)
    (schedule executor actions action-index printer schedule-start workout-start)
    (assoc scheduler
           :executor executor
           :actions actions, :start workout-start, :paused false)))

(defn play [scheduler]
  (send scheduler
        play-fn
        camp.player/stop camp.player/start
        #(stop scheduler)))

(defn paused? [scheduler]
  (:paused @scheduler))

(defn- print-pause [{:keys [printer paused] :as scheduler}]
  (camp.printer/print-pause printer paused)
  scheduler)

(defn- pause-play [{:keys [player] :as scheduler} end-fn]
  (play-fn scheduler util/nop camp.player/pause end-fn))

(defn- pause-stop [{:keys [player start] :as scheduler}]
  (let [elapsed (duration/between start (instant/now))
        s (stop-fn scheduler camp.player/pause)]
    (assoc s, :paused true, :elapsed elapsed)))

(defn- pause-fn [{:keys [paused] :as scheduler} end-fn]
  (print-pause (if paused
                 (pause-play scheduler end-fn)
                 (pause-stop scheduler))))

(defn pause [scheduler]
  (send scheduler pause-fn #(stop scheduler)))

(defn- reset-fn
  ([scheduler] (reset-fn scheduler (:workout scheduler)))
  ([{:keys [player printer paths] :as scheduler} workout]
   (let [tracks (track/tracks workout paths)
         elapsed duration/zero
         actions (create-actions player tracks elapsed #())]
     (camp.printer/print-actions printer actions)
     (print-pause (assoc scheduler
                         :workout workout, :tracks tracks
                         :actions actions, :action-index (atom 0)
                         :paused true, :elapsed elapsed)))))

(defn reset [scheduler & args]
  (stop scheduler)
  (apply send scheduler reset-fn args))

(defn join [scheduler]
  (await scheduler)
  (deref (:shutdown-promise @scheduler)))
