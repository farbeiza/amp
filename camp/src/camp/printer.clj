(ns camp.printer)

(defprotocol Printer
  (print-actions [printer actions])
  (print-state [printer action elapsed])
  (print-summary [printer action])
  (print-pause [printer paused])
  (print-stop [printer]))

(defn- composite-call [f printer & args]
  (doall (map #(apply f % args)
              (:children printer))))

(defrecord CompositePrinter [children]
  Printer
  (print-actions [printer actions]
    (composite-call print-actions printer actions))
  (print-state [printer action elapsed]
    (composite-call print-state printer action elapsed))
  (print-summary [printer action]
    (composite-call print-summary printer action))
  (print-pause [printer paused]
    (composite-call print-pause printer paused))
  (print-stop [printer]
    (composite-call print-stop printer)))
