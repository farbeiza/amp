(ns camp.player
  (:require [amp
             [track :as track]
             [util :as util]]
            [cljc.java-time.duration :as duration]))

(defprotocol Player
  (start [player])
  (stop [player])
  (pause [player])
  (action [player track skip])
  (shutdown [player]))

(defn method-action [time method]
  {:start time, :end time, :method method})

(defn nop [time]
  (method-action time util/nop))

(defn- create-action [player {:keys [start end] :as track} elapsed]
  (let [action-start (.minus start elapsed)
        action-end (.minus end elapsed)
        started (and (.isNegative action-start)
                     (not (.isNegative action-end)))
        skip (if started (.abs action-start) duration/zero)
        action-start (if started duration/zero action-start)]
    (assoc (if (contains? track :song)
             (action player track skip)
             (nop action-start))
           :start action-start, :end action-end
           :track track)))

(defn actions [player tracks elapsed]
  (mapv #(create-action player % elapsed) tracks))

(defn duration [actions]
  (:end (last actions)))
