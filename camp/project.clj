(defproject amp/camp "5.0.1"
  :description "Console Athletic Music Player"
  :plugins [[lein-parent "0.3.8"]
            [lein-cljfmt "0.8.0"]]
  :parent-project {:path "../project.clj"
                   :inherit [:url :license
                             :managed-dependencies
                             :aliases :profiles]}
  :main camp.core
  :dependencies [[amp/libamp]

                 [org.clojure/clojure]

                 [org.clojure/tools.cli]

                 [compojure]
                 [ring]

                 [cljc.java-time]
                 [ch.qos.logback/logback-classic]
                 [org.codehaus.groovy/groovy]
                 [org.slf4j/slf4j-api]]
  :profiles {:cljs {:source-paths ["src"]
                    :dependencies [[org.clojure/clojurescript :scope "provided"]
                                   [thheller/shadow-cljs]
                                   [re-frame]
                                   [day8.re-frame/http-fx]
                                   [reagent]
                                   [cljc.java-time]
                                   [lambdaisland/uri]]}
             :process-test {:source-paths ["script/process_test"]
                            :main camp.process-test}})
