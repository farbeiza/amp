(ns camp.process-test
  (:gen-class)
  (:require [amp
             [configuration :as conf]
             [player :as player]
             [util :as util]
             [vlc :as vlc]]
            [clojure.java.io :as io]))

(defn- append [string & writers]
  (doseq [writer writers]
    (util/appendln writer string)))

(defn- copy-streams [src & dests]
  (let [writers (map io/writer dests)]
    (doseq [line (line-seq (io/reader src))]
      (apply append line writers))))

(defn- run [{:keys [process writer]}]
  (let [output-file (io/file (:log-dir (conf/settings)) "vlc.out")
        handle-input (future (copy-streams (.getInputStream process) *out* output-file))
        handle-output (future (copy-streams *in* *out* writer))]
    (deref handle-input)
    (deref handle-output)))

(defn -main
  "I don't do a whole lot."
  [& args]
  (println "Starting Process Test...")
  (.mkdirs (:log-dir (conf/settings)))
  (let [vlc-player (vlc/vlc)]
    (try (player/start vlc-player)
         (run vlc-player)
         (finally
           (player/stop vlc-player)
           (shutdown-agents))))
  (println "End."))
