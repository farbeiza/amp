import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.FileAppender

import static ch.qos.logback.classic.Level.WARN

def HOME = System.getProperty("user.home")

appender("CONSOLE", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%d{ISO8601,UTC} [%thread] %-5level %logger{36} - %msg%n"
  }
}

appender("FILE", FileAppender) {
  file = "${HOME}/.local/share/amp/amp.log"
  append = false
  encoder(PatternLayoutEncoder) {
    pattern = "%d{ISO8601,UTC} [%thread] %-5level %logger{36} - %msg%n"
  }
}
root(WARN, ["CONSOLE", "FILE"])
