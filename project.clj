(let [project-version "5.0.1"]
  (defproject amp project-version
    :url "https://bitbucket.org/farbeiza/amp"
    :license {:name "MIT License"
              :url "https://opensource.org/licenses/MIT"}
    :managed-dependencies [[org.clojure/clojure "1.10.3"]

                           [amp/libamp ~project-version]

                           [org.clojure/tools.cli "1.0.206"]

                           [lambdaisland/uri "1.10.79"]

                           [compojure "1.6.2"]
                           [ring "1.9.4"]
                           [ring/ring-defaults "0.3.3"]
                           [ring/ring-jetty-adapter "1.9.4"]
                           [ring/ring-json "0.5.1"]
                           [ring/ring-mock "0.4.0"]
                           [ring-partial-content "2.0.1"]
                           [cheshire "5.10.0"]

                           [selmer "1.12.44"]

                           [com.gfredericks/compare "0.1.3"]

                           [org.clojure/tools.logging "1.1.0"]
                           [ch.qos.logback/logback-classic "1.2.5"]
                           [org.codehaus.groovy/groovy "3.0.8"]
                           [org.slf4j/slf4j-api "1.7.32"]

                           [org.clojure/clojurescript "1.11.4"]
                           [thheller/shadow-cljs "2.17.6"]
                           [re-frame "1.3.0-rc2"]
                           [day8.re-frame/http-fx "0.2.4"]
                           [reagent "1.1.0"]
                           [cljc.java-time "0.1.18"]
                           [lambdaisland/uri "1.13.95"]]
    :aliases {"release" ["do"
                         ["clean"]
                         ["cljfmt" "check"]
                         ["test"]
                         ["with-profiles" "cljs" "run" "-m" "shadow.cljs.devtools.cli" "release" "test"]
                         ["install"]
                         ["uberjar"]]
              "debug" ["do"
                       ["install"]
                       ["with-profiles" "uberjar-debug" "uberjar"]]
              "debug-cljs" ["with-profiles" "cljs" "run" "-m" "shadow.cljs.devtools.cli" "compile" "default" "--debug"]
              "release-cljs" ["with-profiles" "cljs" "run" "-m" "shadow.cljs.devtools.cli" "release" "default"]
              "test-cljs" ["with-profiles" "cljs" "run" "-m" "shadow.cljs.devtools.cli" "release" "test"]
              "watch-cljs" ["with-profiles" "cljs" "run" "-m" "shadow.cljs.devtools.cli" "watch" "default"]}
    :profiles {:uberjar {:aot :all
                         :resource-paths ["target/cljs"]
                         :prep-tasks ["clean"
                                      "javac"
                                      "compile"
                                      "release-cljs"]}
               :uberjar-debug {:prep-tasks ["javac"
                                            "compile"
                                            "debug-cljs"]}}))
