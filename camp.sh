#!/bin/bash

SCRIPT_DIR="$(dirname "${0%}")"
SCRIPT_NAME="$(basename "${0}" '.sh')"

UBERJAR=("${SCRIPT_DIR}/target/${SCRIPT_NAME}-"*'-standalone.jar')
UBERJAR="${UBERJAR[-1]}"

java -jar "${UBERJAR}" "${@}"
