#!/bin/bash

name="${1}"
if [ -z "${name}" ]; then
    echo 'Missing name.'

    exit 1
fi

echo -n 'Password: '
read -s password
echo

min_length='8'
if [ "${#password}" -lt "${min_length}" ]; then
    echo "Password should not be shorter than ${min_length} characters."

    exit 2
fi

echo -n 'Confirm password: '
read -s password_confirmation
echo

if [ "${password}" != "${password_confirmation}" ]; then
    echo 'Password and confirmation must match.'

    exit 3
fi

openssl req -x509 -keyout "${name}.key.pem" -out "${name}.cert.pem" \
        -newkey rsa:4096 -days 365 \
        -subj "/C=ES/ST=Cantabria/O=arbeiza.nom.es/OU=Valhalla/CN=${name}.arbeiza.nom.es" \
        -passout file:<(echo "${password}")

openssl pkcs12 -in "${name}.cert.pem" -inkey "${name}.key.pem" -out "${name}.p12" \
        -export -name "${name}.arbeiza.nom.es" \
        -passin file:<(echo "${password}") -passout file:<(echo "${password}")

keytool -importkeystore -destkeystore "${name}.jks" -srckeystore "${name}.p12" -srcstoretype PKCS12 \
        -srcstorepass:file <(echo "${password}") -storepass:file <(echo "${password}")
