{:workouts [{:id "perf-01-easy"
             :name "Perf 01: Easy"
             :color "#28a745"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "A bit harder"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "The summit"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Relax the muscle"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 3, :name "Almost there"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "Grand finale"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "perf-01-hard"
             :name "Perf 01: Hard"
             :color "#dc3545"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "A bit harder"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "The summit"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Relax the muscle"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Almost there"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 4, :name "Grand finale"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "perf-02"
             :name "Perf 02"
             :color "#ffc107"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "A bit harder"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Harder"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "Hardest"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "perf-03"
             :name "Perf 03"
             :color "#ffc107"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "Hard"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "Hard"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "Hard"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "fit-01"
             :name "Fit 01"
             :color "#007bff"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "A bit harder"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Harder"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Valley"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Hard"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "fit-02"
             :name "Fit 02"
             :color "#007bff"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Harder"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Easier"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "A bit harder"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Hard"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "hcal-01"
             :name "Hcal 01"
             :color "#dc3545"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Easier"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Easier"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "hcal-02"
             :name "Hcal 02"
             :color "#dc3545"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Hard"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Hard"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 3, :name "Hard"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "hcal-03"
             :name "Hcal 03"
             :color "#dc3545"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "Hard"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 4, :name "Summit"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 70, :resistance 4, :name "Hard"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Relax"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Finale"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}
            {:id "soft-01"
             :name "Soft 01"
             :color "#28a745"
             :sections [[{:duration #java.time.Duration "PT10S", :name "Get ready"}]
                        [{:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 1, :name "Warm up"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Keep the pace"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Continue"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Go on"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Hang on"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 75, :resistance 2, :name "Persist"}
                         {:duration #java.time.Duration "PT4M17.143S", :tempo 80, :resistance 2, :name "Finale"}]
                        [{:duration #java.time.Duration "PT2M5S", :tempo 60, :name "Cool down"}]]}]

 :tempo-ranges [0.10 0.15 0.20 0.25]
 :refresh-period #java.time.Duration "PT1.0S"

 :skip 0.1
 :skip-last 0.1

 :song-file-re "(?i).*\\.mp3$"
 :db-file-name ".AMP.clj"

 :log-dir "/home/user/.local/share/amp"

 :vlc-command ["vlc" "--intf" "lua"]
 :mp3info-command ["mp3info" "-p" "%F\n%k kB\n%S"]
 :ibt-command ["ibt" "--offline" "--output" "medianTempo"]

 :api-base-uri "/api/"
 :javascript-base-uri ""

 :profiles {:camp {:port 3333}
            :wamp {:port 4444}
            :watch {:port 2222
                    :api-base-uri "http://localhost:2222/api/"
                    :javascript-base-uri "http://localhost:9600/"}}}
